<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230923100014 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE `category` (id INT AUTO_INCREMENT NOT NULL, uuid CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', caption VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_64C19C1D17F50A6 (uuid), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE chat (id INT AUTO_INCREMENT NOT NULL, initiator_id INT NOT NULL, companion_id INT NOT NULL, submission_id INT DEFAULT NULL, status VARCHAR(255) NOT NULL, new_messages_count INT NOT NULL, pinned TINYINT(1) NOT NULL, notification_sent TINYINT(1) DEFAULT NULL, UNIQUE INDEX UNIQ_659DF2AA7DB3B714 (initiator_id), UNIQUE INDEX UNIQ_659DF2AA8227E3FD (companion_id), INDEX IDX_659DF2AAE1FD4933 (submission_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE comment (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, uuid CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', answer_to VARCHAR(255) DEFAULT NULL, likes INT DEFAULT NULL, level INT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_9474526CD17F50A6 (uuid), INDEX IDX_9474526CA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE followers (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, uuid CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', notification_sent TINYINT(1) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_8408FDA7D17F50A6 (uuid), UNIQUE INDEX UNIQ_8408FDA7A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `magazine` (id INT AUTO_INCREMENT NOT NULL, owner_id INT NOT NULL, uuid CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', cover VARCHAR(255) DEFAULT NULL, about_picture VARCHAR(255) DEFAULT NULL, phone_number VARCHAR(255) DEFAULT NULL, about LONGTEXT DEFAULT NULL, primary_contact JSON DEFAULT NULL, type VARCHAR(255) NOT NULL, status VARCHAR(255) NOT NULL, about_content LONGTEXT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_378C2FE4D17F50A6 (uuid), UNIQUE INDEX UNIQ_378C2FE47E3C61F9 (owner_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE magazine_category (id INT AUTO_INCREMENT NOT NULL, category_id INT NOT NULL, magazine_id INT DEFAULT NULL, image_id INT DEFAULT NULL, uuid CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', description LONGTEXT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_39C94B2FD17F50A6 (uuid), UNIQUE INDEX UNIQ_39C94B2F12469DE2 (category_id), INDEX IDX_39C94B2F3EB84A1D (magazine_id), UNIQUE INDEX UNIQ_39C94B2F3DA5256D (image_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE magazine_gallery (id INT AUTO_INCREMENT NOT NULL, magazine_id INT NOT NULL, uuid CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_777F4B9CD17F50A6 (uuid), UNIQUE INDEX UNIQ_777F4B9C3EB84A1D (magazine_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `media` (id INT AUTO_INCREMENT NOT NULL, messages_id INT DEFAULT NULL, uuid CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', file_path VARCHAR(255) DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_6A2CA10CD17F50A6 (uuid), INDEX IDX_6A2CA10CA5905F5A (messages_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE messages (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, chat_id INT NOT NULL, uuid CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', status VARCHAR(255) NOT NULL, text LONGTEXT NOT NULL, emoji VARCHAR(255) DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_DB021E96D17F50A6 (uuid), UNIQUE INDEX UNIQ_DB021E96A76ED395 (user_id), INDEX IDX_DB021E961A9A7125 (chat_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE notification (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, uuid CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', status VARCHAR(255) NOT NULL, type VARCHAR(255) NOT NULL, user_avatar LONGTEXT DEFAULT NULL, user_name LONGTEXT NOT NULL, user_message LONGTEXT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_BF5476CAD17F50A6 (uuid), UNIQUE INDEX UNIQ_BF5476CAA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `photo` (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, category_id INT NOT NULL, media_id INT NOT NULL, magazine_gallery_id INT DEFAULT NULL, comment_id INT DEFAULT NULL, uuid CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', credits VARCHAR(255) DEFAULT NULL, caption VARCHAR(255) NOT NULL, is_sensitive_content TINYINT(1) DEFAULT 0 NOT NULL, likes INT DEFAULT NULL, likes_last_date DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', comments_count INT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_14B78418D17F50A6 (uuid), INDEX IDX_14B78418A76ED395 (user_id), INDEX IDX_14B7841812469DE2 (category_id), UNIQUE INDEX UNIQ_14B78418EA9FDD75 (media_id), INDEX IDX_14B78418417F7EB0 (magazine_gallery_id), INDEX IDX_14B78418F8697D13 (comment_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `speciality` (id INT AUTO_INCREMENT NOT NULL, uuid CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', caption VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_F3D7A08ED17F50A6 (uuid), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE submission (id INT AUTO_INCREMENT NOT NULL, magazine_id INT DEFAULT NULL, uuid CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', status VARCHAR(255) NOT NULL, start_price NUMERIC(10, 2) NOT NULL, end_price NUMERIC(10, 2) DEFAULT NULL, currency VARCHAR(255) NOT NULL, type VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_DB055AF3D17F50A6 (uuid), INDEX IDX_DB055AF33EB84A1D (magazine_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE team_members (id INT AUTO_INCREMENT NOT NULL, magazine_id INT DEFAULT NULL, display_image_id INT DEFAULT NULL, uuid CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', position VARCHAR(255) DEFAULT NULL, display_name VARCHAR(255) DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_BAD9A3C8D17F50A6 (uuid), INDEX IDX_BAD9A3C83EB84A1D (magazine_id), UNIQUE INDEX UNIQ_BAD9A3C85A4F8043 (display_image_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `user` (id INT AUTO_INCREMENT NOT NULL, media_id INT DEFAULT NULL, edit_magazine_id INT DEFAULT NULL, team_members_id INT DEFAULT NULL, followers_id INT DEFAULT NULL, uuid CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', email VARCHAR(180) NOT NULL, account_email VARCHAR(255) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, location VARCHAR(255) NOT NULL, about LONGTEXT NOT NULL, website VARCHAR(255) DEFAULT NULL, instagram VARCHAR(255) DEFAULT NULL, tiktok VARCHAR(255) DEFAULT NULL, pinterest VARCHAR(255) DEFAULT NULL, followers_count INT DEFAULT 0 NOT NULL, followed_count INT DEFAULT 0 NOT NULL, status VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_8D93D649D17F50A6 (uuid), UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), UNIQUE INDEX UNIQ_8D93D649EA9FDD75 (media_id), INDEX IDX_8D93D64924F763B3 (edit_magazine_id), INDEX IDX_8D93D64983D88728 (team_members_id), INDEX IDX_8D93D64915BF9993 (followers_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_speciality (user_id INT NOT NULL, speciality_id INT NOT NULL, INDEX IDX_54B06662A76ED395 (user_id), INDEX IDX_54B066623B5A08D7 (speciality_id), PRIMARY KEY(user_id, speciality_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE messenger_messages (id BIGINT AUTO_INCREMENT NOT NULL, body LONGTEXT NOT NULL, headers LONGTEXT NOT NULL, queue_name VARCHAR(190) NOT NULL, created_at DATETIME NOT NULL, available_at DATETIME NOT NULL, delivered_at DATETIME DEFAULT NULL, INDEX IDX_75EA56E0FB7336F0 (queue_name), INDEX IDX_75EA56E0E3BD61CE (available_at), INDEX IDX_75EA56E016BA31DB (delivered_at), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE chat ADD CONSTRAINT FK_659DF2AA7DB3B714 FOREIGN KEY (initiator_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE chat ADD CONSTRAINT FK_659DF2AA8227E3FD FOREIGN KEY (companion_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE chat ADD CONSTRAINT FK_659DF2AAE1FD4933 FOREIGN KEY (submission_id) REFERENCES submission (id)');
        $this->addSql('ALTER TABLE comment ADD CONSTRAINT FK_9474526CA76ED395 FOREIGN KEY (user_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE followers ADD CONSTRAINT FK_8408FDA7A76ED395 FOREIGN KEY (user_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE `magazine` ADD CONSTRAINT FK_378C2FE47E3C61F9 FOREIGN KEY (owner_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE magazine_category ADD CONSTRAINT FK_39C94B2F12469DE2 FOREIGN KEY (category_id) REFERENCES `category` (id)');
        $this->addSql('ALTER TABLE magazine_category ADD CONSTRAINT FK_39C94B2F3EB84A1D FOREIGN KEY (magazine_id) REFERENCES `magazine` (id)');
        $this->addSql('ALTER TABLE magazine_category ADD CONSTRAINT FK_39C94B2F3DA5256D FOREIGN KEY (image_id) REFERENCES `media` (id)');
        $this->addSql('ALTER TABLE magazine_gallery ADD CONSTRAINT FK_777F4B9C3EB84A1D FOREIGN KEY (magazine_id) REFERENCES `magazine` (id)');
        $this->addSql('ALTER TABLE `media` ADD CONSTRAINT FK_6A2CA10CA5905F5A FOREIGN KEY (messages_id) REFERENCES messages (id)');
        $this->addSql('ALTER TABLE messages ADD CONSTRAINT FK_DB021E96A76ED395 FOREIGN KEY (user_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE messages ADD CONSTRAINT FK_DB021E961A9A7125 FOREIGN KEY (chat_id) REFERENCES chat (id)');
        $this->addSql('ALTER TABLE notification ADD CONSTRAINT FK_BF5476CAA76ED395 FOREIGN KEY (user_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE `photo` ADD CONSTRAINT FK_14B78418A76ED395 FOREIGN KEY (user_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE `photo` ADD CONSTRAINT FK_14B7841812469DE2 FOREIGN KEY (category_id) REFERENCES `category` (id)');
        $this->addSql('ALTER TABLE `photo` ADD CONSTRAINT FK_14B78418EA9FDD75 FOREIGN KEY (media_id) REFERENCES `media` (id)');
        $this->addSql('ALTER TABLE `photo` ADD CONSTRAINT FK_14B78418417F7EB0 FOREIGN KEY (magazine_gallery_id) REFERENCES magazine_gallery (id)');
        $this->addSql('ALTER TABLE `photo` ADD CONSTRAINT FK_14B78418F8697D13 FOREIGN KEY (comment_id) REFERENCES comment (id)');
        $this->addSql('ALTER TABLE submission ADD CONSTRAINT FK_DB055AF33EB84A1D FOREIGN KEY (magazine_id) REFERENCES `magazine` (id)');
        $this->addSql('ALTER TABLE team_members ADD CONSTRAINT FK_BAD9A3C83EB84A1D FOREIGN KEY (magazine_id) REFERENCES `magazine` (id)');
        $this->addSql('ALTER TABLE team_members ADD CONSTRAINT FK_BAD9A3C85A4F8043 FOREIGN KEY (display_image_id) REFERENCES `media` (id)');
        $this->addSql('ALTER TABLE `user` ADD CONSTRAINT FK_8D93D649EA9FDD75 FOREIGN KEY (media_id) REFERENCES `media` (id)');
        $this->addSql('ALTER TABLE `user` ADD CONSTRAINT FK_8D93D64924F763B3 FOREIGN KEY (edit_magazine_id) REFERENCES `magazine` (id)');
        $this->addSql('ALTER TABLE `user` ADD CONSTRAINT FK_8D93D64983D88728 FOREIGN KEY (team_members_id) REFERENCES team_members (id)');
        $this->addSql('ALTER TABLE `user` ADD CONSTRAINT FK_8D93D64915BF9993 FOREIGN KEY (followers_id) REFERENCES followers (id)');
        $this->addSql('ALTER TABLE user_speciality ADD CONSTRAINT FK_54B06662A76ED395 FOREIGN KEY (user_id) REFERENCES `user` (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_speciality ADD CONSTRAINT FK_54B066623B5A08D7 FOREIGN KEY (speciality_id) REFERENCES `speciality` (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE chat DROP FOREIGN KEY FK_659DF2AA7DB3B714');
        $this->addSql('ALTER TABLE chat DROP FOREIGN KEY FK_659DF2AA8227E3FD');
        $this->addSql('ALTER TABLE chat DROP FOREIGN KEY FK_659DF2AAE1FD4933');
        $this->addSql('ALTER TABLE comment DROP FOREIGN KEY FK_9474526CA76ED395');
        $this->addSql('ALTER TABLE followers DROP FOREIGN KEY FK_8408FDA7A76ED395');
        $this->addSql('ALTER TABLE `magazine` DROP FOREIGN KEY FK_378C2FE47E3C61F9');
        $this->addSql('ALTER TABLE magazine_category DROP FOREIGN KEY FK_39C94B2F12469DE2');
        $this->addSql('ALTER TABLE magazine_category DROP FOREIGN KEY FK_39C94B2F3EB84A1D');
        $this->addSql('ALTER TABLE magazine_category DROP FOREIGN KEY FK_39C94B2F3DA5256D');
        $this->addSql('ALTER TABLE magazine_gallery DROP FOREIGN KEY FK_777F4B9C3EB84A1D');
        $this->addSql('ALTER TABLE `media` DROP FOREIGN KEY FK_6A2CA10CA5905F5A');
        $this->addSql('ALTER TABLE messages DROP FOREIGN KEY FK_DB021E96A76ED395');
        $this->addSql('ALTER TABLE messages DROP FOREIGN KEY FK_DB021E961A9A7125');
        $this->addSql('ALTER TABLE notification DROP FOREIGN KEY FK_BF5476CAA76ED395');
        $this->addSql('ALTER TABLE `photo` DROP FOREIGN KEY FK_14B78418A76ED395');
        $this->addSql('ALTER TABLE `photo` DROP FOREIGN KEY FK_14B7841812469DE2');
        $this->addSql('ALTER TABLE `photo` DROP FOREIGN KEY FK_14B78418EA9FDD75');
        $this->addSql('ALTER TABLE `photo` DROP FOREIGN KEY FK_14B78418417F7EB0');
        $this->addSql('ALTER TABLE `photo` DROP FOREIGN KEY FK_14B78418F8697D13');
        $this->addSql('ALTER TABLE submission DROP FOREIGN KEY FK_DB055AF33EB84A1D');
        $this->addSql('ALTER TABLE team_members DROP FOREIGN KEY FK_BAD9A3C83EB84A1D');
        $this->addSql('ALTER TABLE team_members DROP FOREIGN KEY FK_BAD9A3C85A4F8043');
        $this->addSql('ALTER TABLE `user` DROP FOREIGN KEY FK_8D93D649EA9FDD75');
        $this->addSql('ALTER TABLE `user` DROP FOREIGN KEY FK_8D93D64924F763B3');
        $this->addSql('ALTER TABLE `user` DROP FOREIGN KEY FK_8D93D64983D88728');
        $this->addSql('ALTER TABLE `user` DROP FOREIGN KEY FK_8D93D64915BF9993');
        $this->addSql('ALTER TABLE user_speciality DROP FOREIGN KEY FK_54B06662A76ED395');
        $this->addSql('ALTER TABLE user_speciality DROP FOREIGN KEY FK_54B066623B5A08D7');
        $this->addSql('DROP TABLE `category`');
        $this->addSql('DROP TABLE chat');
        $this->addSql('DROP TABLE comment');
        $this->addSql('DROP TABLE followers');
        $this->addSql('DROP TABLE `magazine`');
        $this->addSql('DROP TABLE magazine_category');
        $this->addSql('DROP TABLE magazine_gallery');
        $this->addSql('DROP TABLE `media`');
        $this->addSql('DROP TABLE messages');
        $this->addSql('DROP TABLE notification');
        $this->addSql('DROP TABLE `photo`');
        $this->addSql('DROP TABLE `speciality`');
        $this->addSql('DROP TABLE submission');
        $this->addSql('DROP TABLE team_members');
        $this->addSql('DROP TABLE `user`');
        $this->addSql('DROP TABLE user_speciality');
        $this->addSql('DROP TABLE messenger_messages');
    }
}
