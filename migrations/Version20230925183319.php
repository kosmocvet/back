<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230925183319 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE magazine ADD about_picture_id INT DEFAULT NULL, DROP about_picture');
        $this->addSql('ALTER TABLE magazine ADD CONSTRAINT FK_378C2FE42D7D11D9 FOREIGN KEY (about_picture_id) REFERENCES `media` (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_378C2FE42D7D11D9 ON magazine (about_picture_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE `magazine` DROP FOREIGN KEY FK_378C2FE42D7D11D9');
        $this->addSql('DROP INDEX UNIQ_378C2FE42D7D11D9 ON `magazine`');
        $this->addSql('ALTER TABLE `magazine` ADD about_picture VARCHAR(255) DEFAULT NULL, DROP about_picture_id');
    }
}
