<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230928213256 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE followers ADD followed_user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE followers ADD CONSTRAINT FK_8408FDA7AF2612FD FOREIGN KEY (followed_user_id) REFERENCES `user` (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8408FDA7AF2612FD ON followers (followed_user_id)');
        $this->addSql('ALTER TABLE messages DROP INDEX UNIQ_DB021E96A76ED395, ADD INDEX IDX_DB021E96A76ED395 (user_id)');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D64915BF9993');
        $this->addSql('DROP INDEX IDX_8D93D64915BF9993 ON user');
        $this->addSql('ALTER TABLE user DROP followers_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE followers DROP FOREIGN KEY FK_8408FDA7AF2612FD');
        $this->addSql('DROP INDEX UNIQ_8408FDA7AF2612FD ON followers');
        $this->addSql('ALTER TABLE followers DROP followed_user_id');
        $this->addSql('ALTER TABLE messages DROP INDEX IDX_DB021E96A76ED395, ADD UNIQUE INDEX UNIQ_DB021E96A76ED395 (user_id)');
        $this->addSql('ALTER TABLE `user` ADD followers_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE `user` ADD CONSTRAINT FK_8D93D64915BF9993 FOREIGN KEY (followers_id) REFERENCES followers (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_8D93D64915BF9993 ON `user` (followers_id)');
    }
}
