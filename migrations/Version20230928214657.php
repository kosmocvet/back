<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230928214657 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE chat DROP INDEX UNIQ_659DF2AA8227E3FD, ADD INDEX IDX_659DF2AA8227E3FD (companion_id)');
        $this->addSql('ALTER TABLE chat DROP INDEX UNIQ_659DF2AA7DB3B714, ADD INDEX IDX_659DF2AA7DB3B714 (initiator_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE chat DROP INDEX IDX_659DF2AA7DB3B714, ADD UNIQUE INDEX UNIQ_659DF2AA7DB3B714 (initiator_id)');
        $this->addSql('ALTER TABLE chat DROP INDEX IDX_659DF2AA8227E3FD, ADD UNIQUE INDEX UNIQ_659DF2AA8227E3FD (companion_id)');
    }
}
