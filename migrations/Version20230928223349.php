<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230928223349 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE magazine_magazine_category (magazine_id INT NOT NULL, magazine_category_id INT NOT NULL, INDEX IDX_F33C95C03EB84A1D (magazine_id), INDEX IDX_F33C95C0A465280E (magazine_category_id), PRIMARY KEY(magazine_id, magazine_category_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE magazine_magazine_category ADD CONSTRAINT FK_F33C95C03EB84A1D FOREIGN KEY (magazine_id) REFERENCES `magazine` (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE magazine_magazine_category ADD CONSTRAINT FK_F33C95C0A465280E FOREIGN KEY (magazine_category_id) REFERENCES magazine_category (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE magazine_category DROP FOREIGN KEY FK_39C94B2F3EB84A1D');
        $this->addSql('DROP INDEX IDX_39C94B2F3EB84A1D ON magazine_category');
        $this->addSql('ALTER TABLE magazine_category DROP magazine_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE magazine_magazine_category DROP FOREIGN KEY FK_F33C95C03EB84A1D');
        $this->addSql('ALTER TABLE magazine_magazine_category DROP FOREIGN KEY FK_F33C95C0A465280E');
        $this->addSql('DROP TABLE magazine_magazine_category');
        $this->addSql('ALTER TABLE magazine_category ADD magazine_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE magazine_category ADD CONSTRAINT FK_39C94B2F3EB84A1D FOREIGN KEY (magazine_id) REFERENCES magazine (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_39C94B2F3EB84A1D ON magazine_category (magazine_id)');
    }
}
