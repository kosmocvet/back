<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230928224052 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE magazine ADD magazine_gallery_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE magazine ADD CONSTRAINT FK_378C2FE4417F7EB0 FOREIGN KEY (magazine_gallery_id) REFERENCES magazine_gallery (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_378C2FE4417F7EB0 ON magazine (magazine_gallery_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE `magazine` DROP FOREIGN KEY FK_378C2FE4417F7EB0');
        $this->addSql('DROP INDEX UNIQ_378C2FE4417F7EB0 ON `magazine`');
        $this->addSql('ALTER TABLE `magazine` DROP magazine_gallery_id');
    }
}
