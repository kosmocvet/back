<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230929093944 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE magazine DROP INDEX UNIQ_378C2FE47E3C61F9, ADD INDEX IDX_378C2FE47E3C61F9 (owner_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE `magazine` DROP INDEX IDX_378C2FE47E3C61F9, ADD UNIQUE INDEX UNIQ_378C2FE47E3C61F9 (owner_id)');
    }
}
