<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231016120627 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE followers DROP INDEX UNIQ_8408FDA7AF2612FD, ADD INDEX IDX_8408FDA7AF2612FD (followed_user_id)');
        $this->addSql('ALTER TABLE followers DROP INDEX UNIQ_8408FDA7A76ED395, ADD INDEX IDX_8408FDA7A76ED395 (user_id)');
        $this->addSql('DROP INDEX UNIQ_8408FDA7D17F50A6 ON followers');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE followers DROP INDEX IDX_8408FDA7A76ED395, ADD UNIQUE INDEX UNIQ_8408FDA7A76ED395 (user_id)');
        $this->addSql('ALTER TABLE followers DROP INDEX IDX_8408FDA7AF2612FD, ADD UNIQUE INDEX UNIQ_8408FDA7AF2612FD (followed_user_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8408FDA7D17F50A6 ON followers (uuid)');
    }
}
