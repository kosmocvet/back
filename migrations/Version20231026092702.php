<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231026092702 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE magazine ADD cover_id INT DEFAULT NULL, ADD tiktok LONGTEXT DEFAULT NULL, ADD website LONGTEXT DEFAULT NULL, ADD instagram LONGTEXT DEFAULT NULL, ADD pinterest LONGTEXT DEFAULT NULL, ADD primary_contact_email LONGTEXT DEFAULT NULL, DROP cover');
        $this->addSql('ALTER TABLE magazine ADD CONSTRAINT FK_378C2FE4922726E9 FOREIGN KEY (cover_id) REFERENCES `media` (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_378C2FE4922726E9 ON magazine (cover_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE `magazine` DROP FOREIGN KEY FK_378C2FE4922726E9');
        $this->addSql('DROP INDEX UNIQ_378C2FE4922726E9 ON `magazine`');
        $this->addSql('ALTER TABLE `magazine` ADD cover VARCHAR(255) DEFAULT NULL, DROP cover_id, DROP tiktok, DROP website, DROP instagram, DROP pinterest, DROP primary_contact_email');
    }
}
