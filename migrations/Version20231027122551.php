<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231027122551 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE magazine_user (magazine_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_8D9B3AF53EB84A1D (magazine_id), INDEX IDX_8D9B3AF5A76ED395 (user_id), PRIMARY KEY(magazine_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_magazine (user_id INT NOT NULL, magazine_id INT NOT NULL, INDEX IDX_D701CBE4A76ED395 (user_id), INDEX IDX_D701CBE43EB84A1D (magazine_id), PRIMARY KEY(user_id, magazine_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE magazine_user ADD CONSTRAINT FK_8D9B3AF53EB84A1D FOREIGN KEY (magazine_id) REFERENCES `magazine` (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE magazine_user ADD CONSTRAINT FK_8D9B3AF5A76ED395 FOREIGN KEY (user_id) REFERENCES `user` (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_magazine ADD CONSTRAINT FK_D701CBE4A76ED395 FOREIGN KEY (user_id) REFERENCES `user` (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_magazine ADD CONSTRAINT FK_D701CBE43EB84A1D FOREIGN KEY (magazine_id) REFERENCES `magazine` (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D64924F763B3');
        $this->addSql('DROP INDEX IDX_8D93D64924F763B3 ON user');
        $this->addSql('ALTER TABLE user DROP edit_magazine_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE magazine_user DROP FOREIGN KEY FK_8D9B3AF53EB84A1D');
        $this->addSql('ALTER TABLE magazine_user DROP FOREIGN KEY FK_8D9B3AF5A76ED395');
        $this->addSql('ALTER TABLE user_magazine DROP FOREIGN KEY FK_D701CBE4A76ED395');
        $this->addSql('ALTER TABLE user_magazine DROP FOREIGN KEY FK_D701CBE43EB84A1D');
        $this->addSql('DROP TABLE magazine_user');
        $this->addSql('DROP TABLE user_magazine');
        $this->addSql('ALTER TABLE `user` ADD edit_magazine_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE `user` ADD CONSTRAINT FK_8D93D64924F763B3 FOREIGN KEY (edit_magazine_id) REFERENCES magazine (id)');
        $this->addSql('CREATE INDEX IDX_8D93D64924F763B3 ON `user` (edit_magazine_id)');
    }
}
