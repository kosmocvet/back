<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231030115409 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE credit_name (id INT AUTO_INCREMENT NOT NULL, photo_id INT NOT NULL, user_id INT DEFAULT NULL, uuid CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', name VARCHAR(100) DEFAULT NULL, instagram VARCHAR(100) DEFAULT NULL, UNIQUE INDEX UNIQ_59E1C13BD17F50A6 (uuid), INDEX IDX_59E1C13B7E9E4C8C (photo_id), INDEX IDX_59E1C13BA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE credit_name ADD CONSTRAINT FK_59E1C13B7E9E4C8C FOREIGN KEY (photo_id) REFERENCES `photo` (id)');
        $this->addSql('ALTER TABLE credit_name ADD CONSTRAINT FK_59E1C13BA76ED395 FOREIGN KEY (user_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE magazine DROP about_content');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE credit_name DROP FOREIGN KEY FK_59E1C13B7E9E4C8C');
        $this->addSql('ALTER TABLE credit_name DROP FOREIGN KEY FK_59E1C13BA76ED395');
        $this->addSql('DROP TABLE credit_name');
        $this->addSql('ALTER TABLE `magazine` ADD about_content LONGTEXT DEFAULT NULL');
    }
}
