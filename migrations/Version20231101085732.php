<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231101085732 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE followers ADD followed_magazine_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE followers ADD CONSTRAINT FK_8408FDA7663C24B9 FOREIGN KEY (followed_magazine_id) REFERENCES `magazine` (id)');
        $this->addSql('CREATE INDEX IDX_8408FDA7663C24B9 ON followers (followed_magazine_id)');
        $this->addSql('ALTER TABLE magazine ADD followers_count INT DEFAULT 0 NOT NULL');
        $this->addSql('ALTER TABLE photo DROP credits');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE `photo` ADD credits VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE followers DROP FOREIGN KEY FK_8408FDA7663C24B9');
        $this->addSql('DROP INDEX IDX_8408FDA7663C24B9 ON followers');
        $this->addSql('ALTER TABLE followers DROP followed_magazine_id');
        $this->addSql('ALTER TABLE `magazine` DROP followers_count');
    }
}
