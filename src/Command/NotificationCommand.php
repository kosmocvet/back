<?php

namespace App\Command;

use App\Entity\Chat;
use App\Entity\Followers;
use App\Entity\Messages;
use App\Entity\Notification;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: 'app:create-notification')]
class NotificationCommand extends Command
{
    public function __construct(private EntityManagerInterface $entityManager)
    {
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('New notifications.')
            ->setHelp('Эта команда создает новое уведомление.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $chats = $this->entityManager->getRepository(Chat::class)->findBy([
            'status' => 'unread',
        ]);

        foreach ($chats as $chat) {
            $message = $this->entityManager->getRepository(Messages::class)->findOneBy(
                ['chat' => $chat],
                ['createdAt' => 'DESC']
            );
            if (null !== $message) {
                $sender = $message->getUser();
                $companion = $chat->getCompanion();
                $initiator = $chat->getInitiator();

                if ($sender === $initiator) {
                    $receiver = $companion;
                } else {
                    $receiver = $initiator;
                }

                $notification = new Notification();
                $notification->setUser($receiver);
                $notification->setUserMessage('New message from '.$sender->getName());
                $notification->setType('message');
                $notification->setStatus('unread');
                $notification->setUserName($sender->getName());
                if (null !== $sender->getAvatar()) {
                    $notification->setUserAvatar($sender->getAvatar()->getImageUrl());
                }
                $chat->setNotificationSent(true);
                $this->entityManager->persist($notification);
                $this->entityManager->persist($chat);
                $this->entityManager->flush();
            }
        }

        /** @var Followers[] $followers */
        $followers = $this->entityManager->getRepository(Followers::class)->findBy([
            'notificationSent' => false,
        ]);

        foreach ($followers as $follower) {
            $notification = new Notification();
            $notification->setUser($follower->getUser());
            $notification->setUserMessage('New follower '.$follower->getFollowedUser()->getName());
            $notification->setType('follower');
            $notification->setStatus('unread');
            $notification->setUserName($follower->getUser()->getName());
            if (null !== $follower->getUser()->getAvatar()) {
                $notification->setUserAvatar($follower->getUser()->getAvatar()->getImageUrl());
            }
            $follower->setNotificationSent(true);
            $this->entityManager->persist($notification);
            $this->entityManager->persist($follower);
            $this->entityManager->flush();
        }

        return Command::SUCCESS;
    }
}
