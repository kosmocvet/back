<?php

namespace App\Controller\Auth;

use App\Entity\User;
use App\Service\UserTokenService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

final class AddEditorRole extends AbstractController
{

    public function __construct(
        private readonly Security $security,
        private readonly EntityManagerInterface $entityManager,
        private readonly UserTokenService $tokenService,
        private readonly UserPasswordHasherInterface $passwordHasher,
    ) {
    }

    public function __invoke(Request $request)
    {
        $user = $this->security->getUser();
        $roles = [];
        $roles[] = 'ROLE_USER';
        $roles[] = 'ROLE_MAGAZINE_EDITOR';
        $user->setRoles($roles);
        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return new JsonResponse('You are an editor');
    }
}
