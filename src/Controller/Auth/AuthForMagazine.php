<?php

namespace App\Controller\Auth;

use App\Entity\User;
use App\Service\UserTokenService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

final class AuthForMagazine extends AbstractController
{

    public function __construct(
        private readonly Security $security,
        private readonly EntityManagerInterface $entityManager,
        private readonly UserTokenService $tokenService,
        private readonly UserPasswordHasherInterface $passwordHasher,
    ) {
    }

    public function __invoke(Request $request)
    {

        $data = json_decode($request->getContent(), true);
        if (!isset($data['email'])) {
            return new JsonResponse('Please, enter an email',404);
        }
        if (!isset($data['password'])) {
            return new JsonResponse('Please, enter a password',404);
        }

        [$email, $password] = array_values($data);

        $userRepository = $this->entityManager->getRepository(User::class);
        $user = $userRepository->findOneBy(['email' => $email]);
        if (!isset($user)) {
                return new JsonResponse('User with that email is not exists',404);
        }
        if (!$this->passwordHasher->isPasswordValid($user, $password)) {
                return new JsonResponse('Wrong password was given',401);
        }
        $roles = $user->getRoles();

        if (!in_array('ROLE_MAGAZINE_EDITOR', $roles) && !in_array('ROLE_MAGAZINE_OWNER', $roles)) {
            return new JsonResponse('You are neither a magazine editor nor an owner',401);
        }

        $token = $this->tokenService->getActiveToken($user)->getToken();

        return new JsonResponse(['token' => $token],200);

    }
}
