<?php

namespace App\Controller\Chat;

use App\Entity\Chat;
use App\Entity\Messages;
use App\Entity\User;
use App\Service\ChatsGetService;
use App\Service\GetInterlocutorService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * CreatePhotoController class.
 */
#[AsController]
final class GetInterlocutor extends AbstractController
{
    /**
     * Constructor.
     */
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly Security $security,
        private readonly GetInterlocutorService $interlocutorService
    ) {
    }

    public function __invoke(Request $request): UnauthorizedHttpException|JsonResponse
    {
        $chatuuid = (json_decode($request->getContent(), true));

        if (!$this->security->getUser()) {
            throw new UnauthorizedHttpException('Bearer', 'You are not a user.');
        }
        if (!isset($chatuuid['chatuuid'])) {
            return new JsonResponse('chatuuid is not specified', 404);
        }

        $chatuuid = (json_decode($request->getContent(), true))['chatuuid'];
        
        return $this->interlocutorService->getInterlocutor($this->security->getUser()->getUserIdentifier(),$chatuuid);
        
    }
}
