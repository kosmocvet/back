<?php

namespace App\Controller\CheckAccess;

use App\Model\PageModel;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class CheckAccess extends AbstractController
{
    // TODO: NEED TEST
    public function __invoke(
        Request $request,
        AuthorizationCheckerInterface $authChecker
    ): JsonResponse {
        $pageUrl = $request->get('page');

        $somePageModel = new PageModel($pageUrl);

        if ($authChecker->isGranted('PAGE_ACCESS', $somePageModel)) {
            return new JsonResponse(['role' => 'Regular User']);
        } else {
            return new JsonResponse(['role' => 'Visitor']);
        }
    }
}
