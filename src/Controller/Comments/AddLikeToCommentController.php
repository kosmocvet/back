<?php

namespace App\Controller\Comments;

use App\Entity\Comment;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Lock\LockFactory;
use Symfony\Component\Security\Core\Security;

/**
 * CreatePhotoController class.
 */
#[AsController]
final class AddLikeToCommentController extends AbstractController
{
    /**
     * Constructor.
     */
    public function __construct(
        private readonly Security $security,
        private readonly EntityManagerInterface $entityManager,
//        private readonly LockFactory $lockFactory,
    ) {
    }

    public function __invoke(Request $request): JsonResponse
    {
        if (!$this->security->getUser()) {
            throw new UnauthorizedHttpException('Bearer', 'You are not a user.');
        }

        $user = $this->entityManager->getRepository(User::class)->findOneBy([
            'email' => $this->security->getUser()->getUserIdentifier(),
        ]);

        $data = json_decode($request->getContent(), true);

        if (isset($data['commentId'])) {
            $commentId = $data['commentId'];
        } else {
            throw new BadRequestHttpException('The "commentId" is required. Please, select a photoId .');
        }
//        $lock = $this->lockFactory->createLock($user->getId().$commentId, 604800);
//        if ($lock->acquire()) {
            $comment = $this->entityManager->getRepository(Comment::class)->findOneBy([
                'uuid' => $commentId,
            ]);

            if (!$comment) {
                throw new BadRequestHttpException('Comment not found.');
            }

            $comment->setLikes($comment->getLikes() + 1);

            $this->entityManager->persist($comment);
            $this->entityManager->flush();
//            $lock->release();

            return new JsonResponse(['message' => 'Like added'], 200);
//        } else {
//            return new JsonResponse(['error' => 'You can only like once a week'], 403);
//        }
    }
}
