<?php

namespace App\Controller\Comments;

use App\Entity\Comment;
use App\Entity\Photo;
use App\Event\Comment\CreateCommentEvent as CommentCreateCommentEvent;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * CreatePhotoController class.
 */
#[AsController]
final class CreateCommentByPhotoIdController extends AbstractController
{
    /**
     * Constructor.
     */
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly Security $security,
    ) {
    }

    public function __invoke(Request $request,EventDispatcherInterface $eventDispatcher): UnauthorizedHttpException|JsonResponse
    {
        if (!$this->security->getUser()) {
            throw new UnauthorizedHttpException('Bearer', 'You are not a user.');
        }

        $data = json_decode($request->getContent(), true);

        if (null == $data['photoUuid']) {
            throw new BadRequestHttpException('Missing photo Uuid parameter.');
        }

        $photo = $this->entityManager->getRepository(Photo::class)->findOneBy([
            'uuid' => $data['photoUuid'],
        ]);

        
        $comment = new Comment();
        $comment->setPhoto($photo);
        $comment->setUser($this->security->getUser());
        $comment->setText($data['text']);
        if (isset($data['parent'])) {
            $parent = $this->entityManager->getRepository(Comment::class)->findOneBy([
                'uuid' => $data['parent'],
            ]);
            if (!$parent) {
                throw new BadRequestHttpException('Parent comment not found.');
            }
            $comment->setParent($parent);
        }

        $this->entityManager->persist($comment);
        $this->entityManager->flush();

        $event = new CommentCreateCommentEvent($photo,$this->entityManager);
        $eventDispatcher->dispatch($event, 'comment.created');

        return new JsonResponse(['success' => true], 200);
    }
}
