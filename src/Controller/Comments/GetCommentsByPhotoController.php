<?php

namespace App\Controller\Comments;

use App\Entity\Comment;
use App\Entity\Photo;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * CreatePhotoController class.
 */
#[AsController]
final class GetCommentsByPhotoController extends AbstractController
{
    /**
     * Constructor.
     */
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly Security $security,
        private readonly SerializerInterface $serializer
    ) {
    }

    public function __invoke(Request $request): UnauthorizedHttpException|JsonResponse
    {
        if (!$this->security->getUser()) {
            throw new UnauthorizedHttpException('Bearer', 'You are not a user.');
        }
        if (null == $request->query->get('photoId')) {
            throw new BadRequestHttpException('Missing photo Id parameter.');
        }
        $photo = $this->entityManager->getRepository(Photo::class)->findOneBy(
            ['uuid' => $request->query->get('photoId')]
        );
        $comments = $this->entityManager->getRepository(Comment::class)->findBy(
            ['photo' => $photo, 'parent' => null],
            ['createdAt' => 'DESC']
        );
        if (!$comments) {
            return new JsonResponse(['message' => 'No comments'], 200);
        }

        $comments = $this->serializer->serialize($comments, 'json', ['groups' => 'comment:read']);

        $comments = json_decode($comments, true);

                foreach ($comments as $comment) {
                    if (!$comment['children']) {
                        unset($comment['children']);
                    }
                }

        return new JsonResponse($comments, 200);
    }
}
