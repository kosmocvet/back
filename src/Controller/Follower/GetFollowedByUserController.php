<?php

namespace App\Controller\Follower;

use App\Entity\Followers;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * CreatePhotoController class.
 */
#[AsController]
final class GetFollowedByUserController extends AbstractController
{
    /**
     * Constructor.
     */
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly Security $security,
        private readonly SerializerInterface $serializer
    ) {
    }

    public function __invoke(Request $request): UnauthorizedHttpException|JsonResponse
    {
        $name = $request->query->get('name') ?? '';
        if (!$this->security->getUser()) {
            throw new UnauthorizedHttpException('Bearer', 'You are not a user.');
        }
        if (null == $request->query->get('userId')) {
            throw new BadRequestHttpException('Missing userId parameter.');
        }
        $user = $this->entityManager->getRepository(User::class)->findOneBy([
                'uuid' => $request->query->get('userId'),
        ]);
       
        if (!$user) {
            return new JsonResponse(['user with that uuid isn\'t exists'],404);
        }

        $followersRepository = $this->entityManager->getRepository(Followers::class);


        $followed = $followersRepository->createQueryBuilder('f')
                ->join('f.followedUser', 'fu')
                ->where('f.user = :userId')
                ->andWhere('fu.name LIKE :followedUserName')
                ->setParameter('userId', $user->getId())
                ->setParameter('followedUserName', '%' . $name . '%')            
                ->getQuery()
                ->getResult();

        $response = [];
        
        if (!$followed) {
            return new JsonResponse([],200);
        }
        foreach($followed as $el)
        {
            $followedUser = $el->getFollowedUser();
            $user = $this->serializer->serialize($followedUser, 'json', ['groups' => 'followers:read:extended']);
            $user = json_decode($user, true);
            $response[] = $user;
            
        }

        return new JsonResponse($response, 200);
    }
}
