<?php

namespace App\Controller\Follower;

use App\Entity\Followers;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Serializer\SerializerInterface;


/**
 * CreatePhotoController class.
 */
#[AsController]
final class GetFollowersByUserController extends AbstractController
{
    /**
     * Constructor.
     */
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly Security $security,
        private readonly SerializerInterface $serializer
    ) {
    }

    public function __invoke(Request $request): UnauthorizedHttpException|JsonResponse
    {
        $name = $request->query->get('name') ?? '';
        if (!$this->security->getUser()) {
            throw new UnauthorizedHttpException('Bearer', 'You are not a user.');
        }
        if (null == $request->query->get('userId')) {
            throw new BadRequestHttpException('Missing userId parameter.');
        }
        $user = $this->entityManager->getRepository(User::class)->findOneBy([
                'uuid' => $request->query->get('userId'),
        ]);
       
        if (!$user) {
            return new JsonResponse(['user with that uuid isn\'t exists'],404);
        }

        $followersRepository = $this->entityManager->getRepository(Followers::class);


        $followers = $followersRepository->createQueryBuilder('f')
                ->join('f.user', 'fu')
                ->where('f.followedUser = :userId')
                ->setParameter('userId', $user->getId())
                ->andWhere('fu.name LIKE :followerName')
                ->setParameter('followerName', '%' . $name . '%')            
                ->getQuery()
                ->getResult();
        $response = [];
        if (!$followers) {
            return new JsonResponse([],200);
        }
        foreach($followers as $el)
        {
            
            $followerForUser = $el->getUser();
            $user = $this->serializer->serialize($followerForUser, 'json', ['groups' => 'followers:read:extended']);
            $user = json_decode($user, true);
            $response[] = $user;

        }

        return new JsonResponse($response, 200);


    }
}
