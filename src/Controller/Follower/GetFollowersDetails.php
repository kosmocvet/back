<?php

namespace App\Controller\Follower;

use App\Entity\Followers;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Security\Core\Security;

/**
 * CreatePhotoController class.
 */
#[AsController]
final class GetFollowersDetails extends AbstractController
{
    /**
     * Constructor.
     */
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly Security $security
    ) {
    }

    public function __invoke(Request $request): UnauthorizedHttpException|JsonResponse
    {
        $response = [];
        if (!$this->security->getUser()) {
            throw new UnauthorizedHttpException('Bearer', 'You are not a user.');
        }
        if (null == $request->query->get('userId')) {
            throw new BadRequestHttpException('Missing userId parameter.');
        }
        $user = $this->entityManager->getRepository(User::class)->findOneBy([
                'uuid' => $request->query->get('userId'),
            ]);
            
        $userFromRequest = $this->entityManager->getRepository(User::class)->findOneBy([
            'email' => $this->security->getUser()->getUserIdentifier(),
        ]);

        if (!$user) {
            return new JsonResponse('User with this uuid isn\'t exists' , 404);
        }

        $isFollowed = $this->entityManager->getRepository(Followers::class)->findOneBy([
            'user' => $userFromRequest->getId(),
            'followedUser' => $user->getId()
        ]);

        $response['followersCount'] = $user->getFollowersCount();
        $response['followedCount'] = $user->getFollowedCount();
        $response['isFollowed'] = $isFollowed === null ? false : true;

        return new JsonResponse($response, 200);
    }
}
