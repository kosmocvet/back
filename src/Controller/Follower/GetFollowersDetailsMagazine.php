<?php

namespace App\Controller\Follower;

use App\Entity\Followers;
use App\Entity\Magazine;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Security\Core\Security;

/**
 * CreatePhotoController class.
 */
#[AsController]
final class GetFollowersDetailsMagazine extends AbstractController
{
    /**
     * Constructor.
     */
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly Security $security
    ) {
    }

    public function __invoke(Request $request): UnauthorizedHttpException|JsonResponse
    {
        $response = [];
        if (!$this->security->getUser()) {
            throw new UnauthorizedHttpException('Bearer', 'You are not a user.');
        }
        if (null == $request->query->get('magazineId')) {
            throw new BadRequestHttpException('Missing userId parameter.');
        }
        $magazine = $this->entityManager->getRepository(Magazine::class)->findOneBy([
                'uuid' => $request->query->get('magazineId'),
            ]);
            
        $userFromRequest = $this->entityManager->getRepository(User::class)->findOneBy([
            'email' => $this->security->getUser()->getUserIdentifier(),
        ]);

        if (!$magazine) {
            return new JsonResponse('User with this uuid isn\'t exists' , 404);
        }

        $isFollowed = $this->entityManager->getRepository(Followers::class)->findOneBy([
            'user' => $userFromRequest->getId(),
            'followedMagazine' => $magazine->getId()
        ]);

        $response['followersCount'] = $magazine->getFollowersCount();
        $response['isFollowed'] = $isFollowed === null ? false : true;

        return new JsonResponse($response, 200);
    }
}
