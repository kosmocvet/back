<?php

namespace App\Controller\Follower;

use App\Entity\Followers;
use App\Entity\User;
use App\Event\Followers\FollowersUnFollowEvent;
use App\Service\UnFollowMagazineService;
use App\Service\UnFollowUserService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * CreatePhotoController class.
 */
#[AsController]
final class UnfollowController extends AbstractController
{
    /**
     * Constructor.
     */
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly Security $security,
        private readonly UnFollowUserService $unFollowUserService,
        private readonly UnFollowMagazineService $unFollowMagazineService,
    ) {
    }

    public function __invoke(Request $request): UnauthorizedHttpException|JsonResponse
    {

        if (!$this->security->getUser()) {
            throw new UnauthorizedHttpException('Bearer', 'You are not a user.');
        }
        if (!$request->query->get('userId') && !$request->query->get('magazineId')) {
            throw new BadRequestHttpException('Missing userId or magazineId parameter.');
        }
        if ($request->query->get('userId') && $request->query->get('magazineId')) {
            throw new BadRequestHttpException('You can\'t specify 2 params at one time.');
        }

        $user = $this->entityManager->getRepository(User::class)->findOneBy([
            'email' => $this->security->getUser()->getUserIdentifier(),
        ]);


        if($request->query->get('userId')) return $this->unFollowUserService->unfollow($request->query->get('userId'),$user);
        if($request->query->get('magazineId')) return $this->unFollowMagazineService->unfollow($request->query->get('magazineId'),$user);

    }
}
