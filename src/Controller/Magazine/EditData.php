<?php

namespace App\Controller\Magazine;

use App\Entity\User;
use App\Service\MagazineUpdateService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * CreatePhotoController class.
 */
#[AsController]
final class EditData extends AbstractController
{
    /**
     * Constructor.
     */
    public function __construct(
        private readonly Security $security,
        private readonly SerializerInterface $serializer,
        private readonly EntityManagerInterface $em,
        private readonly MagazineUpdateService $mu,
    ) {
    }

    public function __invoke(Request $request): UnauthorizedHttpException|JsonResponse
    {
        $data = json_decode($request->getContent(),true);

        if (!isset($data['uuid'])) {
            return new JsonResponse('You missed an uuid param',404);
        }

        $email = $this->security->getUser()->getUserIdentifier();

        $user = $this->em->getRepository(User::class)->findOneBy(['email' => $email]);

        $magazine = $this->em
            ->createQueryBuilder()
            ->select('m') 
            ->from('App\Entity\Magazine', 'm')
            ->leftJoin('m.editors', 'e')
            ->leftJoin('m.owner', 't')
            ->where('t.id = :user')
            ->orWhere('e.id = :user')
            ->setParameter('user',  $user->getId())
            ->andWhere('m.uuid = :uuid')
            ->setParameter('uuid', $data['uuid'])
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();

        if (!$magazine) {
            return new JsonResponse('Any magazine with that uuid is allowed for you',401);
        }

        return $this->mu->updateMagazine($data,$magazine);

    }
}
