<?php

namespace App\Controller\Magazine;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * CreatePhotoController class.
 */
#[AsController]
final class GetDataForEdits extends AbstractController
{
    /**
     * Constructor.
     */
    public function __construct(
        private readonly Security $security,
        private readonly SerializerInterface $serializer,
        private readonly EntityManagerInterface $em,
    ) {
    }

    public function __invoke(Request $request): UnauthorizedHttpException|JsonResponse
    {
        $magazineId = $request->query->get('magazineId');

        $email = $this->security->getUser()->getUserIdentifier();

        $user = $this->em->getRepository(User::class)->findOneBy(['email' => $email]);

        $queryBuilder = $this->em
            ->createQueryBuilder()
            ->select('m') 
            ->from('App\Entity\Magazine', 'm')
            ->leftJoin('m.editors', 'e')
            ->leftJoin('m.owner', 't')
            ->where('t.id = :user')
            ->orWhere('e.id = :user')
            ->setParameter('user',  $user->getId());
            
        if ($magazineId) {
            $queryBuilder
                ->andWhere('m.uuid = :uuid')
                ->setParameter('uuid',  $magazineId);
        }

       $magazine = $queryBuilder
            ->orderBy('m.updatedAt', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();


        $response = $this->serializer->serialize($magazine,'json',['groups' => 'magazine:read:extended']);
        $response = json_decode($response,true);

        if (!$response) {
           return new JsonResponse('any magazine to edit',404);
        }

        return new JsonResponse($response, 200);

    }
}
