<?php

namespace App\Controller\Magazine;

use App\Repository\MagazineRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * CreatePhotoController class.
 */
#[AsController]
final class GetMagazineWithFilterController extends AbstractController
{
    /**
     * Constructor.
     */
    public function __construct(
        private readonly Security $security,
        private readonly SerializerInterface $serializer,
        private readonly MagazineRepository $magazineRepository,
    ) {
    }

    public function __invoke(Request $request): UnauthorizedHttpException|JsonResponse
    {
        if (!$this->security->getUser()) {
            throw new UnauthorizedHttpException('Bearer', 'You are not a user.');
        }

        $category = $request->get('category' , 'EMPTY');
        $type = $request->get('type' , 'EMPTY');

        $query = $this->magazineRepository->createQueryBuilder('f')
        ->join('f.magazineCategories', 'fu')
        ->join('fu.category', 'fc')
        ->andWhere('f.status =:status')
        ->setParameter('status', 'published');         

        if ($category !== 'EMPTY') {
            $query
            ->andWhere('fc.caption =:caption')
            ->setParameter('caption', $category);         
        }
        if ($type !== 'EMPTY') {
            $query
                ->andWhere('f.type = :type')
                ->setParameter('type', $type);
        }

        $magazines = $query ->getQuery()->getResult();

        $data = $this->serializer->serialize($magazines, 'json', ['groups' => 'magazinefilter:read']);

        if (!$data) {
            return new JsonResponse(['message' => 'Magazines not found'], 404);
        }

        $data = json_decode($data,true);

        return new JsonResponse($data, 200);
    }
}
