<?php

namespace App\Controller\Magazine;

use App\Entity\Chat;
use App\Entity\Magazine;
use App\Entity\Submission;
use App\Entity\Subscription;
use App\Entity\SubscriptionPlan;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * CreatePhotoController class.
 */
#[AsController]
final class GetOneMagazine extends AbstractController
{
    /**
     * Constructor.
     */
    public function __construct(
        private readonly Security $security,
        private readonly SerializerInterface $serializer,
        private readonly EntityManagerInterface $em,
    ) {
    }

    public function __invoke(Request $request): UnauthorizedHttpException|JsonResponse
    {
        if (!$this->security->getUser()) {
            return new JsonResponse(['success' => false], 400);
        }

        $email = $this->security->getUser()->getUserIdentifier();

        $user = $this->em->getRepository(User::class)->findOneBy(['email' => $email]);

        if (!$user) {
            return new JsonResponse('not a user', 404);
        }

        $magazine = $this->em->getRepository(Magazine::class)->findOneBy(['uuid' => $request->query->get('magazineId')]);

        if (!$magazine) {
            return new JsonResponse('Any magazine with that uuid', 404);
        }

        $magazine = $this->serializer->serialize($magazine ,'json', ['groups' => 'magazine:one:read']);
        $magazine = json_decode($magazine,true);
        
        if (isset($magazine['submissions'])) {
            for ($i=0; $i < count($magazine['submissions']); $i++) { 
                $submission = $this->em->getRepository(Submission::class)->findBy(['uuid' => $magazine['submissions'][$i]['uuid']]);
                $chat = $this->em->getRepository(Chat::class)->findBy(['submission' => $submission, 'initiator'=> $user]);
                $chat = $this->serializer->serialize($chat ,'json', ['groups' => 'magazine:one:read']);
                $chat = json_decode($chat,true);
                $magazine['submissions'][$i]['chat'] = $chat;
            }
        }

        
        return new JsonResponse($magazine, 200);

    }
}
