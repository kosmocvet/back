<?php

namespace App\Controller\Magazine;

use App\Entity\Magazine;
use App\Entity\Subscription;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * CreatePhotoController class.
 */
#[AsController]
final class MagazineBilling extends AbstractController
{
    /**
     * Constructor.
     */
    public function __construct(
        private readonly Security $security,
        private readonly SerializerInterface $serializer,
        private readonly EntityManagerInterface $em,
    ) {
    }

    public function __invoke(Request $request): UnauthorizedHttpException|JsonResponse
    {
        if (!$this->security->getUser()) {
            return new JsonResponse(['success' => false], 400);
        }
        /** @var User $user */

        $email = $this->security->getUser()->getUserIdentifier();

        $user = $this->em->getRepository(User::class)->findOneBy(['email' => $email]);

        if (!$user) {
            return new JsonResponse('Not a user', 401);
        }

        $magazine = $this->em
            ->createQueryBuilder()
            ->select('m') 
            ->from('App\Entity\Magazine', 'm')
            ->leftJoin('m.editors', 'e')
            ->leftJoin('m.owner', 't')
            ->where('t.id = :user')
            ->orWhere('e.id = :user')
            ->setParameter('user',  $user->getId())
            ->andWhere('m.uuid = :uuid')
            ->setParameter('uuid', $request->query->get('magazineId'))
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();


        if (!$magazine) {
            return new JsonResponse('magazine with that uuid is not exists', 404);
        }

        $subscription = $this->em->getRepository(Subscription::class)->findOneBy(['magazine' => $magazine, 'status' => 'active']);

        if (!$subscription) {
            return new JsonResponse([], 200);
        }

        $response['uuid'] = $subscription->getUuid();
        $response['plan']['name'] = $subscription->getPlan()->getName();
        $response['plan']['cost'] = $subscription->getPlan()->getCost();
        $response['status'] = $subscription->getStatus();
        $response['renews'] = $subscription->getRenews()->format('Y-m-d');

        return new JsonResponse($response, 200);
    }
}
