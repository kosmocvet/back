<?php

namespace App\Controller\Magazine;

use App\Entity\Magazine;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * CreatePhotoController class.
 */
#[AsController]
final class MagazineList extends AbstractController
{
    /**
     * Constructor.
     */
    public function __construct(
        private readonly Security $security,
        private readonly SerializerInterface $serializer,
        private readonly EntityManagerInterface $em,
    ) {
    }

    public function __invoke(Request $request): UnauthorizedHttpException|JsonResponse
    {

        $email = $this->security->getUser()->getUserIdentifier();

        $user = $this->em->getRepository(User::class)->findOneBy(['email' => $email]);

        $magazines = $this->em
            ->createQueryBuilder()
            ->select('m') 
            ->from('App\Entity\Magazine', 'm')
            ->leftJoin('m.editors', 'e')
            ->leftJoin('m.owner', 't')
            ->where('t.id = :user')
            ->orWhere('e.id = :user')
            ->setParameter('user',  $user->getId())
            ->orderBy('m.updatedAt', 'DESC')
            ->getQuery()
            ->getResult();
        
        $response = [];

        foreach($magazines as $magazine)
        {
            $isEditor = $this->em
            ->createQueryBuilder()
            ->select('m') 
            ->from('App\Entity\Magazine', 'm')
            ->leftJoin('m.editors', 'e')
            ->andWhere('e.id = :user')
            ->andWhere('m.id = :magazine')
            ->setParameter('user',  $user->getId())
            ->setParameter('magazine',  $magazine->getId())
            ->getQuery()
            ->getResult();

            $response[] = [
                'uuid' => $magazine->getUuid(),
                'name' => $magazine->getName(),
                'role'=> $isEditor ? 'ROLE_MAGAZINE_EDITOR' : 'ROLE_MAGAZINE_OWNER'
            ];
        }

        if (!$magazines) {
           return new JsonResponse([],200);
        }

        return new JsonResponse($response, 200);

    }
}
