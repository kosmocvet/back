<?php

namespace App\Controller\Magazine;

use App\Entity\Magazine;
use App\Entity\Subscription;
use App\Entity\SubscriptionPlan;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * CreatePhotoController class.
 */
#[AsController]
final class MagazineSubscribePlan extends AbstractController
{
    /**
     * Constructor.
     */
    public function __construct(
        private readonly Security $security,
        private readonly SerializerInterface $serializer,
        private readonly EntityManagerInterface $em,
    ) {
    }

    public function __invoke(Request $request): UnauthorizedHttpException|JsonResponse
    {
        if (!$this->security->getUser()) {
            return new JsonResponse(['success' => false], 400);
        }

        $magazineUuid = $request->query->get('magazineId' , '');

        $magazine = $this->em->getRepository(Magazine::class)->findOneBy(['uuid' => $magazineUuid]);

        if (!$magazine) {
            return new JsonResponse('Any magazine with that uuid',404);
        }

        $plans = $this->em->getRepository(SubscriptionPlan::class)->findAll();
        $subscription = $this->em->getRepository(Subscription::class)->findOneBy(['magazine' => $magazine, 'status' => 'active']);

        $response = [];

        foreach ($plans as $plan) {
            if ('year' === $plan->getPeriod()) {
                $response[] = [
                    'uuid' => $plan->getUuid(),
                    'name' => $plan->getName(),
                    'description' => $plan->getDescription(),
                    'cost' => $plan->getCost(),
                    'type' => $plan->getType(),
                    'currentPlan' => $subscription?->getPlan()?->getUuid() === $plan?->getUuid(),
                ];
            }
        }

        return new JsonResponse($response, 200);

    }
}
