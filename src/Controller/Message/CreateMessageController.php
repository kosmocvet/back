<?php

namespace App\Controller\Message;

use App\Entity\Chat;
use App\Entity\Media;
use App\Entity\Messages;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Security\Core\Security;

/**
 * CreatePhotoController class.
 */
#[AsController]
final class CreateMessageController extends AbstractController
{
    /**
     * Constructor.
     */
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly Security $security
    ) {
    }

    public function __invoke(Request $request): UnauthorizedHttpException|JsonResponse
    {
        if (!$this->security->getUser()) {
            throw new UnauthorizedHttpException('Bearer', 'You are not a user.');
        }
        $user = $this->entityManager->getRepository(User::class)->findOneBy([
                'email' => $this->security->getUser()->getUserIdentifier(),
            ]);

        $data = json_decode($request->getContent(), true);
        if (isset($data['chatId'])) {
            $chatId = $data['chatId'];
        } else {
            throw new BadRequestHttpException('The "chatId" is required. Please, select a chatId .');
        }

        $chat = $this->entityManager->getRepository(Chat::class)->findOneBy([
            'uuid' => $chatId,
        ]);
        if ($user !== $chat->getInitiator() && $user !== $chat->getCompanion()) {
            throw new BadRequestHttpException('You have no access to this chat.');
        }

        $message = new Messages();
        $message->setUser($user);
        $message->setChat($chat);
        if(isset($data['emoji'])){
            $message->setEmoji($data['emoji']);
        }
        if(isset($data['text'])){
            $message->setText($data['text']);
        }
        $message->setStatus('unread');
        $message->setCreatedAt(new \DateTime());
        $message->setUpdatedAt(new \DateTime());
        if (isset($data['attachments'])) {
            foreach ($data['attachments'] as $item) {
                $media = $this->entityManager->getRepository(Media::class)->findOneBy([
                    'uuid' => $item,
                ]);
                if (null !== $media) {
                    $message->addAttachment($media);
                }
            }
        }

        $this->entityManager->persist($message);
        $this->entityManager->flush();

        $dateTime = new \DateTime(); 
        $timezone = new \DateTimeZone('Europe/Kiev'); 
        $dateTime->setTimezone($timezone); 

        $chat->setUpdatedAt($dateTime);

        $this->entityManager->persist($chat);
        $this->entityManager->flush();

        return new JsonResponse(['status' => true], 200);
    }
}
