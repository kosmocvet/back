<?php

namespace App\Controller\Message;

use App\Entity\Chat;
use App\Entity\Messages;
use App\Entity\User;
use App\Service\MessagesGetService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * CreatePhotoController class.
 */
#[AsController]
final class GetMessagesByChatController extends AbstractController
{
    /**
     * Constructor.
     */
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly Security $security,
        private MessagesGetService $getMessagesService
    ) {
    }

    public function __invoke(Request $request): UnauthorizedHttpException|JsonResponse
    {
        if (!$this->security->getUser()) {
            throw new UnauthorizedHttpException('Bearer', 'You are not a user.');
        }
        $user = $this->entityManager->getRepository(User::class)->findOneBy([
                'email' => $this->security->getUser()->getUserIdentifier(),
            ]);

        $chatId = $request->get('chatId');
        if (!$chatId) {
            throw new BadRequestHttpException('The "chatId" is required. Please, select a chatId .');
        }

        $chat = $this->entityManager->getRepository(Chat::class)->findOneBy([
            'uuid' => $chatId,
        ]);
        if (null == $chat) {
            throw new BadRequestHttpException('The chat with this id does not exist.');
        }
        if ($user !== $chat->getInitiator() && $user !== $chat->getCompanion()) {
            throw new BadRequestHttpException('You have no access to this chat.');
        }

        $path = $request->getPathInfo();
        
        $page = $request->query->get('page','noPageSpecified');
        $messages = $this->getMessagesService->getMessages($page,$chat,$user,$chatId,$path);

        return new JsonResponse($messages, 200);
    }
}
