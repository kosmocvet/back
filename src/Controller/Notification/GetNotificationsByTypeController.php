<?php

namespace App\Controller\Notification;

use App\Entity\Notification;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * CreatePhotoController class.
 */
#[AsController]
final class GetNotificationsByTypeController extends AbstractController
{
    /**
     * Constructor.
     */
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly Security $security,
        private readonly SerializerInterface $serializer
    ) {
    }

    public function __invoke(Request $request): UnauthorizedHttpException|JsonResponse
    {
        $followers = [];
        if (!$this->security->getUser()) {
            throw new UnauthorizedHttpException('Bearer', 'You are not a user.');
        }
        if (null == $request->query->get('type')) {
            throw new BadRequestHttpException('Missing type parameter.');
        }
        $notifications = $this->entityManager->getRepository(Notification::class)->findBy([
                'type' => $request->query->get('type'),
                'user' => $this->security->getUser(),
            ]);

        $notifications = $this->serializer->serialize($notifications, 'json', ['groups' => 'notifications:read']);

        $notifications = json_decode($notifications, true);

        return new JsonResponse($notifications, 200);
    }
}
