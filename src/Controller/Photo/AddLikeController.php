<?php

namespace App\Controller\Photo;

use App\Entity\Photo;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Lock\LockFactory;
use Symfony\Component\Lock\Store\SemaphoreStore;
use Symfony\Component\Security\Core\Security;

/**
 * CreatePhotoController class.
 */
#[AsController]
final class AddLikeController extends AbstractController
{
    /**
     * Constructor.
     */
    public function __construct(
        private readonly Security $security,
        private readonly EntityManagerInterface $entityManager,
    ) {
    }

    public function __invoke(Request $request): JsonResponse
    {
        if (!$this->security->getUser()) {
            throw new UnauthorizedHttpException('Bearer', 'You are not a user.');
        }

        $user = $this->entityManager->getRepository(User::class)->findOneBy([
            'email' => $this->security->getUser()->getUserIdentifier(),
        ]);

        $data = json_decode($request->getContent(), true);

        if (isset($data['photoId'])) {
            $photoId = $data['photoId'];
        } else {
            throw new BadRequestHttpException('The "photoId" is required. Please, select a photoId .');
        }
        //        $store = new SemaphoreStore();
        //        $lockFactory = new LockFactory($store);
        //
        //        $lock = $lockFactory->createLock(self::class.$user->getId().$photoId, 604800);
        //        if ($lock->acquire()) {

        $photo = $this->entityManager->getRepository(Photo::class)->findOneBy([
            'uuid' => $photoId,
        ]);

        if (!$photo) {
            throw new BadRequestHttpException('Photo not found.');
        }

        $photo->setLikes($photo->getLikes() + 1);

        $this->entityManager->persist($photo);
        $this->entityManager->flush();

        //            $lock->release();

                    return new JsonResponse(['message' => 'Like added'], 200);
        //        } else {
        //            return new JsonResponse(['error' => 'You can only like once a week'], 403);
        //        }
    }
}
