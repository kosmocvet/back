<?php

namespace App\Controller\Photo;

use App\Entity\Media;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Security\Core\Security;

/**
 * CreatePhotoController class.
 */
#[AsController]
final class CreateMediaController extends AbstractController
{
    /**
     * Constructor.
     */
    public function __construct(
        private readonly Security $security,
        private readonly EntityManagerInterface $entityManager,
    ) {
    }

    public function __invoke(Request $request): JsonResponse
    {
        $uploadedFile = $request->files->get('file');
        if (!$uploadedFile) {
            throw new BadRequestHttpException('The "file" is required. Please, select a file.');
        }
        if (!$this->security->getUser()) {
            throw new UnauthorizedHttpException('Bearer', 'You are not a user.');
        }
        $photo = new Media();
        $photo->file = $uploadedFile;
        $this->entityManager->persist($photo);
        $this->entityManager->flush();
        $imageUrl = "/media/{$photo->getFilePath()}";

        return new JsonResponse([
            'fileId' => $photo->getUuid(),
            'id' => $photo->getId(),
            'imageUrl' => $imageUrl,
        ], 200);
    }
}
