<?php

namespace App\Controller\Photo;

use App\Entity\Category;
use App\Entity\CreditName;
use App\Entity\Magazine;
use App\Entity\Media;
use App\Entity\Photo;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * CreatePhotoController class.
 */
#[AsController]
final class CreatePhotoController extends AbstractController
{
    /**
     * Constructor.
     */
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly Security $security,
        private readonly SerializerInterface $serializer,
    ) {
    }

    public function __invoke(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        $category = $data['category'];
        $mediaId = $data['fileId'];
        $magazineUuid = $data['magazine'];

        if (!$mediaId) {
            throw new BadRequestHttpException('The "fileId" is required. Please, select a fileId .');
        }
        if (!$this->security->getUser()) {
            throw new UnauthorizedHttpException('Bearer', 'You are not a user.');
        }
        $user = $this->entityManager->getRepository(User::class)->findOneBy([
            'email' => $this->security->getUser()->getUserIdentifier()]);

        /** @var Category $category */
        $category = $this->entityManager->getRepository(Category::class)->findOneBy([
            'uuid' => $category,
        ]);
        if (null == $category) {
            throw new BadRequestHttpException('The "category" is required. Please, select a category .');
        }
        /** @var Media $photoFile */
        $photoFile = $this->entityManager->getRepository(Media::class)->findOneBy([
            'uuid' => $mediaId,
        ]);

        if (null == $photoFile) {
            throw new BadRequestHttpException('The "fileId" is required. Please, select a fileId .');
        }

        $magazine = $this->entityManager->getRepository(Magazine::class)->findOneBy([
            'uuid' => $magazineUuid,
        ]);

        if (!$magazine && $magazineUuid) {
            throw new BadRequestHttpException('Failed to find magazine with that uuid.');
        }

        $this->entityManager->beginTransaction();
        try {      
            $photo = new Photo();
            $photo->setMagazine($magazine);
            $photo->setCaption($data['caption']);
            $photo->setUser($user);
            $photo->setCategory($category);
            $photo->setIsSensitiveContent((bool) $data['is_sensitive_content']);
            $photo->setMedia($photoFile);
            $photo->imageUrl = "/media/{$photoFile->getFilePath()}";
            $this->entityManager->persist($photo);
            $this->entityManager->flush();
            
            if (isset($data['creators'])) 
            {
                $this->verifyCreator($data['creators'],$photo);
            }

            $this->entityManager->commit();
            
        } catch (\Exception $e) {
            $this->entityManager->rollback();

            throw $e;
        }

        $normalizedPhotos = $this->serializer->normalize($photo, null, ['groups' => 'photo:read']);

        return new JsonResponse($normalizedPhotos, 200);
    }

    public function verifyCreator($creators,$photo)
    {
        foreach ($creators as $el) {

            if (!$el['name']) {
                throw new BadRequestHttpException('name is required.');
            }
            
            $el['user'] ? $user = $this->entityManager->getRepository(User::class)->findOneBy(['uuid' => $el['user'] ]) : $user = null;

            if ($el['user'] && !$user) {
                throw new BadRequestHttpException('user is not found.');
            }

            $credits = new CreditName();
            $credits->setUser($user);
            $credits->setName($el['name']);
            $credits->setPhoto($photo);

            $this->entityManager->persist($credits);
            $this->entityManager->flush();
                
        }
    }


}
