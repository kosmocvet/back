<?php

namespace App\Controller\Photo;

use App\Entity\Photo;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * CreatePhotoController class.
 */
#[AsController]
final class GetPhotoByIdController extends AbstractController
{
    /**
     * Constructor.
     */
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly SerializerInterface $serializer,
    ) {
    }

    public function __invoke(Request $request): JsonResponse
    {
        $id = $request->get('id');
        $photo = $this->entityManager->getRepository(Photo::class)->find($id);

        if (!$photo) {
            return new JsonResponse(['error' => 'Photo not found'], 404);
        }

        $normalizedPhoto = $this->serializer->normalize($photo, null, ['groups' => 'photo:read']);

        return new JsonResponse($normalizedPhoto, 200);
    }
}
