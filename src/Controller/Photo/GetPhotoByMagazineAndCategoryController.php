<?php

namespace App\Controller\Photo;

use App\Entity\Category;
use App\Entity\Magazine;
use App\Entity\Photo;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * CreatePhotoController class.
 */
#[AsController]
final class GetPhotoByMagazineAndCategoryController extends AbstractController
{
    /**
     * Constructor.
     */
    public function __construct(
        private readonly Security $security,
        private readonly EntityManagerInterface $entityManager,
        private readonly SerializerInterface $serializer
    ) {
    }

    public function __invoke(Request $request): JsonResponse
    {
        if (!$this->security->getUser()) {
            throw new UnauthorizedHttpException('Bearer', 'You are not a user.');
        }
        $magazineId = $request->query->get('magazineUuid');
        $categoryId = $request->query->get('categoryUuid');

        $magazine = $this->entityManager->getRepository(Magazine::class)->findOneBy([
            'uuid' => $magazineId,
        ]);

        $category = $this->entityManager->getRepository(Category::class)->findOneBy([
            'uuid' => $categoryId,
        ]);

        $photos = $this->entityManager->getRepository(Photo::class)->findBy([
            'magazine' => $magazine,
            'category' => $category,
        ]);

        $normalizedPhotos = $this->serializer->normalize($photos, null, ['groups' => 'photo:read']);

        return new JsonResponse($normalizedPhotos, 200);
    }
}
