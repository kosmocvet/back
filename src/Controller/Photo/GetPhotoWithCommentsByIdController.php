<?php

namespace App\Controller\Photo;

use App\Entity\Comment;
use App\Entity\Photo;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * CreatePhotoController class.
 */
#[AsController]
final class GetPhotoWithCommentsByIdController extends AbstractController
{
    /**
     * Constructor.
     */
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly SerializerInterface $serializer,
        private readonly Security $security
    ) {
    }

    public function __invoke(Request $request): JsonResponse
    {
        $id = $request->query->get('uuid');
        $photo = $this->entityManager->getRepository(Photo::class)->findOneBy(['uuid' => $id]);

        if (null === $photo) {
            return new JsonResponse(['error' => 'Photo not found'], 404);
        }

        $user = $this->entityManager->getRepository(User::class)->findOneBy([
            'email' => $this->security->getUser()->getUserIdentifier(),
        ]);

        $comments = $this->entityManager->getRepository(Comment::class)->findBy(
            ['photo' => $photo, 'parent' => null],
            ['createdAt' => 'DESC']);
        foreach ($comments as $comment) {
            if (!$comment->getChildren()) {
                $comment->setChildren(null);
            }
        }


        $photo = $this->serializer->normalize($photo, 'json', ['groups' => ['photoComments:read']]);
        $photo['comments'] = $this->serializer->normalize($comments, 'jsonld', ['groups' => ['comment:read']]);
        $photo['commenter'] = $this->serializer->normalize($user, 'json', ['groups' => ['photo:read']]);
        

        return new JsonResponse($photo, 200);
    }
}
