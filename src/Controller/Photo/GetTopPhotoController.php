<?php

namespace App\Controller\Photo;

use App\Entity\Photo;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Serializer\SerializerInterface;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;

/**
 * CreatePhotoController class.
 */
#[AsController]
final class GetTopPhotoController extends AbstractController
{
    /**
     * Constructor.
     */
    public function __construct(
        private readonly Security $security,
        private readonly EntityManagerInterface $entityManager,
        private readonly SerializerInterface $serializer
    ) {
    }

    public function __invoke(Request $request): JsonResponse
    {
        if (!$this->security->getUser()) {
            throw new UnauthorizedHttpException('Bearer', 'You are not a user.');
        }
        $user = $this->entityManager->getRepository(User::class)->findOneBy([
            'email' => $this->security->getUser()->getUserIdentifier()]);

        $photos = $this->entityManager->getRepository(Photo::class)->findBy(
            [],
            ['createdAt' => 'DESC'],
        );
        //            ->findPhotosByUserAndLikes($user->getId());

        $normalizedPhotos = $this->serializer->normalize($photos, null, ['groups' => 'photo:read']);

        /**
         * @var Photo $photo
         */
        foreach ($photos as $index => $photo) {
            $normalizedPhotos[$index]['imageUrl'] = "/media/{$photo->getMedia()->getFilePath()}";
        }

        return new JsonResponse($normalizedPhotos, 200);
    }
}
