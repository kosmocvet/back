<?php

namespace App\Controller;

use App\Service\UserTokenService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class SecurityController extends AbstractController
{
    public function __construct(private UserTokenService $userTokenService)
    {
    }

    #[Route('/auth', name: 'app_login', methods: 'POST')]
    public function login(): JsonResponse
    {
        if (!$this->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->json([
                'error' => 'Invalid login request: check that the Content-Type header is "application/json".',
            ], 400);
        }

        return $this->json([
                'token' => $this->userTokenService->getActiveToken()
            ]
        );
    }

}