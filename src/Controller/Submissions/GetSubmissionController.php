<?php

namespace App\Controller\Submissions;

use App\Entity\Magazine;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * CreatePhotoController class.
 */
#[AsController]
final class GetSubmissionController extends AbstractController
{
    /**
     * Constructor.
     */
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly Security $security,
        private readonly SerializerInterface $serializer
    ) {
    }

    public function __invoke(Request $request): UnauthorizedHttpException|JsonResponse
    {
        if (!$this->security->getUser()) {
            throw new UnauthorizedHttpException('Bearer', 'You are not a user.');
        }

        $magazineId = $request->get('magazineId');

        if (!isset($magazineId)) {
            return new JsonResponse(['message' => 'Magazine ID not found'], 404);
        }

        $magazines = $this->entityManager->getRepository(Magazine::class)->findOneBy([
            'uuid' => $magazineId,
        ]);

        if (null === $magazines) {
            return new JsonResponse(['message' => 'Magazine not found'], 404);
        }

        $submissions = $magazines->getSubmissions();

        $data = $this->serializer->serialize($submissions, 'json', ['groups' => 'submission:read']);

        $data = json_decode($data, true);

        return new JsonResponse($data, 200);
    }
}
