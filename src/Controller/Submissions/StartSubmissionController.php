<?php

namespace App\Controller\Submissions;

use App\Entity\Chat;
use App\Entity\Media;
use App\Entity\Messages;
use App\Entity\Submission;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Security\Core\Security;

/**
 * CreatePhotoController class.
 */
#[AsController]
final class StartSubmissionController extends AbstractController
{
    /**
     * Constructor.
     */
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly Security $security,
    ) {
    }

    public function __invoke(Request $request): array|JsonResponse
    {
        if (!$this->security->getUser()) {
            throw new UnauthorizedHttpException('Bearer', 'You are not a user.');
        }

        $data = json_decode($request->getContent(), true);
        $initiator = $this->entityManager->getRepository(User::class)->findOneBy([
            'email' => $this->security->getUser()->getUserIdentifier()]);

        if (isset($data['media'])) {
            $media = $data['media'];
        } else {
            return new JsonResponse(['message' => 'Media not found'], 404);
        }
        $submission = $this->entityManager->getRepository(Submission::class)->findOneBy([
            'uuid' => $data['submissionId'],
        ]);
        if (null === $submission) {
            return new JsonResponse(['message' => 'Submission not found'], 404);
        }

        $companion = $submission->getMagazine()->getOwner();
        // Создаем новый Chat
        $chat = new Chat();
        $chat->setCompanion($companion);
        $chat->setStatus('new');
        $chat->setNewMessagesCount(1);
        $chat->setPinned(false);
        $chat->setInitiator($initiator);
        $chat->setSubmission($submission);

        $this->entityManager->persist($chat);

        // Создаем новое сообщение
        $message = new Messages();
        $message->setChat($chat);
        $message->setStatus('new');
        $message->setUser($initiator);
        foreach ($media as $photoId) {
            $photo = $this->entityManager->getRepository(Media::class)->findOneBy([
                'uuid' => $photoId,
            ]);
            if (null === $photo) {
                return new JsonResponse(['message' => 'Media '.$photoId.' not found'], 404);
            }

            $message->addAttachment($photo);
        }

        $this->entityManager->persist($message);
        $this->entityManager->flush();

        return new JsonResponse(['chatId' => $chat->getUuid()], 200);
    }
}
