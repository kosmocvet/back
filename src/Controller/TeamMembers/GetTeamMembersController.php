<?php

namespace App\Controller\TeamMembers;

use App\Entity\Magazine;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * CreatePhotoController class.
 */
#[AsController]
final class GetTeamMembersController extends AbstractController
{
    /**
     * Constructor.
     */
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly Security $security,
        private readonly SerializerInterface $serializer
    ) {
    }

    public function __invoke(Request $request): UnauthorizedHttpException|JsonResponse
    {
        if (!$this->security->getUser()) {
            throw new UnauthorizedHttpException('Bearer', 'You are not a user.');
        }

        $magazineId = $request->get('magazineId');

        $magazines = $this->entityManager->getRepository(Magazine::class)->findOneBy([
            'uuid' => $magazineId,
        ]);

        if (null === $magazines) {
            return new JsonResponse(['message' => 'Magazine not found'], 404);
        }

        $teamMembers = $magazines->getTeamMembers();

        $data = $this->serializer->serialize($teamMembers, 'json', ['groups' => 'teamMembers:read']);

        return new JsonResponse($data, 200);
    }
}
