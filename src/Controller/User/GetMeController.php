<?php

namespace App\Controller\User;

use App\Entity\User;
use App\Repository\PhotoRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Serializer\SerializerInterface;

class GetMeController extends AbstractController
{
    public function __invoke(
        EntityManagerInterface $manager,
        Security $security,
        SerializerInterface $serializer,
        PhotoRepository $photoRepository,
    ): JsonResponse {
        if (null == $security->getUser()) {
            return new JsonResponse(['success' => false], 400);
        }
        $user = $manager->getRepository(User::class)->findOneBy([
            'email' => $security->getUser()->getUserIdentifier(),
        ]);
        if (!$user) {
            return new JsonResponse(['success' => false], 404);
        }

        $userData = $serializer->normalize($user, null, ['groups' => 'user:read']);

        if (isset($userData['specialities'])) {
            $data = [];
            foreach ($user->getSpecialities() as $speciality) {
                $data[] = [
                    'uuid' => $speciality->getUuid(),
                    'caption' => $speciality->getCaption(),
                ];
            }
            $userData['specialities'] = $data;
        }

        $photos = $photoRepository->findBy(
            ['user' => $user],
            ['createdAt' => 'DESC'],
        );

        $photosUrls = [];
        foreach ($photos as $photo) {
            $photosUrls[] = [
                'uuid' => $photo->getUuid(),
                'imageUrl' => "/media/{$photo->getMedia()->getFilePath()}",
                'isSensitiveContent' => $photo->getIsSensitiveContent(),
            ];
        }


        $userData['photos'] = $photosUrls;

        $userData['isEditor'] = in_array('ROLE_MAGAZINE_EDITOR', $user->getRoles()) ? true : false;

        if (isset($userData['magazine'])) {
            $magazine = $user->getMagazine();
            if ($magazine) {
                $userData['magazine'] = $magazine->getUuid();
            }
        }

        return new JsonResponse($userData, 200);
    }
}
