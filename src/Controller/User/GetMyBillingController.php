<?php

namespace App\Controller\User;

use App\Entity\Subscription;
use App\Entity\User;
use App\Repository\PhotoRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Serializer\SerializerInterface;

class GetMyBillingController extends AbstractController
{
    public function __invoke(
        EntityManagerInterface $manager,
        Security $security,
        SerializerInterface $serializer,
        PhotoRepository $photoRepository,
    ): JsonResponse {
        if (null == $security->getUser()) {
            return new JsonResponse(['success' => false], 400);
        }
        /** @var User $user */
        $user = $manager->getRepository(User::class)->findOneBy([
            'email' => $security->getUser()->getUserIdentifier(),
        ]);
        if (!$user) {
            return new JsonResponse(['success' => false], 200);
        }
        $subscription = $manager->getRepository(Subscription::class)->findOneBy(['user' => $user, 'status' => 'active']);
        if (!$subscription) {
            return new JsonResponse(['subscription' => null, 'paymentMethod' => null], 200);
        }

        $response['subscription']['uuid'] = $subscription->getUuid();
        $response['subscription']['plan']['name'] = $subscription->getPlan()->getName();
        $response['subscription']['plan']['cost'] = $subscription->getPlan()->getCost();
        $response['subscription']['status'] = $subscription->getStatus();
        $response['subscription']['renews'] = $subscription->getRenews()->format('Y-m-d');

        $response['paymentMethod'][] = null;

        return new JsonResponse($response, 200);
    }
}
