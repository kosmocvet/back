<?php

namespace App\Controller\User;

use App\Entity\Subscription;
use App\Entity\SubscriptionPlan;
use App\Entity\User;
use App\Repository\PhotoRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Serializer\SerializerInterface;

class GetMySubscriptionPlansController extends AbstractController
{
    public function __invoke(
        EntityManagerInterface $manager,
        Security $security,
        SerializerInterface $serializer,
        PhotoRepository $photoRepository,
    ): JsonResponse {
        if (null == $security->getUser()) {
            return new JsonResponse(['success' => false], 400);
        }
        /** @var User $user */
        $user = $manager->getRepository(User::class)->findOneBy([
            'email' => $security->getUser()->getUserIdentifier(),
        ]);
        if (!$user) {
            return new JsonResponse(['success' => false], 404);
        }
        $plans = $manager->getRepository(SubscriptionPlan::class)->findAll();

        $subscription = $manager->getRepository(Subscription::class)->findOneBy(['user' => $user, 'status' => 'active']);
        $responseNoActive = [];
        foreach ($plans as $plan) {
            if ('month' === $plan->getPeriod()) {
                $responseNoActive[] = [
                    'uuid' => $plan->getUuid(),
                    'name' => $plan->getName(),
                    'description' => $plan->getDescription(),
                    'cost' => $plan->getCost(),
                    'period' => $plan->getPeriod(),
                    'type' => $plan->getType(),
                    'currentPlan' => false,
                ];
            }
        }
        if (!$subscription) {
            return new JsonResponse($responseNoActive, 200);
        }

        $plans = $manager->getRepository(SubscriptionPlan::class)->findAll();

        $response = [];

        foreach ($plans as $plan) {
            if ('month' === $plan->getPeriod()) {
                $response[] = [
                    'uuid' => $plan->getUuid(),
                    'name' => $plan->getName(),
                    'description' => $plan->getDescription(),
                    'cost' => $plan->getCost(),
                    'period' => $plan->getPeriod(),
                    'type' => $plan->getType(),
                    'currentPlan' => $subscription->getPlan()->getUuid() === $plan->getUuid(),
                ];
            }
        }

        return new JsonResponse($response, 200);
    }
}
