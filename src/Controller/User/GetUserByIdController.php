<?php

namespace App\Controller\User;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Serializer\SerializerInterface;

class GetUserByIdController extends AbstractController
{
    public function __invoke(Request $request,
        EntityManagerInterface $manager,
        Security $security,
        SerializerInterface $serializer
    ): JsonResponse|AccessDeniedHttpException|BadRequestHttpException {
//        if (null == $security->getUser()) {
//            return new AccessDeniedHttpException('You are not a user.');
//        }
        $user = $manager->getRepository(User::class)->findOneBy([
            'uuid' => $request->get('id'),
        ]);
        if (!$user) {
            return new BadRequestHttpException('User not found.');
        }

        $userData = $serializer->normalize($user, null, ['groups' => 'user:read']);

        return new JsonResponse($userData, 200);
    }
}
