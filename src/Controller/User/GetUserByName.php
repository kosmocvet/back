<?php

namespace App\Controller\User;

use App\Entity\Followers;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * CreatePhotoController class.
 */
#[AsController]
final class GetUserByName extends AbstractController
{
    /**
     * Constructor.
     */
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly Security $security,
        private readonly SerializerInterface $serializer
    ) {
    }

    public function __invoke(Request $request): UnauthorizedHttpException|JsonResponse
    {
        $name = $request->query->get('name' , '');

        $followersRepository = $this->entityManager->getRepository(User::class);


        $users = $followersRepository->createQueryBuilder('u')
                ->andWhere('u.name LIKE :name')
                ->setParameter('name', '%' . $name . '%')
                ->setMaxResults(10)
                ->getQuery()
                ->getResult();
        
        if (!$users) {
            return new JsonResponse([],200);
        }

        $response = $this->serializer->serialize($users, 'json', ['groups' => 'username:read']);
        $response = json_decode($response,true);

        return new JsonResponse($response, 200);
    }
}
