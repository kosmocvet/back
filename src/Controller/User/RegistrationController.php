<?php

namespace App\Controller\User;

use App\Service\RegistrationrService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class RegistrationController extends AbstractController
{
    public function __construct(private RegistrationrService $registrationrService)
    {
    }

    public function __invoke(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        $response = $this->registrationrService->registration($data);
        if ('success' !== $response) {

            return new JsonResponse([$response], 200);
        }

        return new JsonResponse(['success' => true], 200);
    }
}
