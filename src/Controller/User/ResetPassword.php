<?php

namespace App\Controller\User;

use App\Entity\User;
use App\Event\User\ResetPassword as UserResetPassword;
use App\Service\EmailSender;
use App\Service\PasswordGeneratorService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class ResetPassword extends AbstractController
{
    public function __construct(
    private EntityManagerInterface $em,
    private UserPasswordHasherInterface $hasher,
    private readonly PasswordGeneratorService $passwordGeneratorService,
    private readonly EmailSender $emailSender,
    )
    {
    }

    public function __invoke(Request $request,EventDispatcherInterface $eventDispatcher): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        if (!isset($data['email'])) {
            return new JsonResponse('Email is not specified' , 404);
        }

        $user = $this->em->getRepository(User::class)->findOneBy(['email' => $data['email']]);

        if (!$user) {
            return new JsonResponse('Account not found' , 401);
        }
        $event = new UserResetPassword($user,$this->em,$this->hasher,$this->passwordGeneratorService,$this->emailSender);
        $eventDispatcher->dispatch($event, 'user.reset');


        return new JsonResponse(['success' => true], 200);
    }
}
