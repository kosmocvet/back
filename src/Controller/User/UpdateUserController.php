<?php

namespace App\Controller\User;

use App\Service\RegistrationrService;
use App\Service\UpdateUserService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class UpdateUserController extends AbstractController
{
    public function __construct(private UpdateUserService $updateUserService)
    {
    }

    public function __invoke(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        $response = $this->updateUserService->update($data);
        if ('success' !== $response) {
            return new JsonResponse([$response], 400);
        }

        return new JsonResponse(['success' => true], 200);
    }
}
