<?php

namespace App\DataFixtures;

use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

/**
 * CategoryFixtures class.
 */
class CategoryFixtures extends Fixture
{
    public const CATEGORIES = [
        'Fashion',
        'Beauty',
        'Portrait',
        'Nude',
        'Travel',
        'Sport',
        'Wedding',
        'Natural',
    ];
    public const CATEGORY_REFERENCE = 'category_';

    public function load(ObjectManager $manager): void
    {
        foreach (self::CATEGORIES as $key => $categoriesCaption) {
            $category = new Category();
            $category->setCaption($categoriesCaption);
            $category->setCreatedAt(new \DateTimeImmutable());
            $category->setUpdatedAt(new \DateTime());
            $this->addReference(self::CATEGORY_REFERENCE.$key, $category);

            $manager->persist($category);
        }

        $manager->flush();
        // Load Speciality of users.
    }
}
