<?php

namespace App\DataFixtures;

use App\Entity\Speciality;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class SpecialityFixtures extends Fixture
{
    public const SPECIALITY_ARRAY = [
        'Photographer',
        'Model',
        'Makeup Artist',
        'Hair Stylist',
        'Fashion Designer',
        'Retoucher',
        'Editor',
        'Creative Director',
        'Wardrobe Stylist',
        'Publication',
        'Agency',
        'PR manager',
        'Producer',
        'Film director',
    ];

    public const SPECIALITY_REFERENCE = 'speciality_';

    public function load(ObjectManager $manager): void
    {
        foreach (self::SPECIALITY_ARRAY as $i => $value) {
            $speciality = new Speciality();
            $speciality->setCaption($value);
            $speciality->setCreatedAt(new \DateTime());
            $speciality->setUpdatedAt(new \DateTime());
            $manager->persist($speciality);

            // Добавляем референс, который можно использовать в других фикстурах
            $this->addReference(self::SPECIALITY_REFERENCE.$i, $speciality);
        }

        $manager->flush();
    }
}
