<?php

namespace App\Doctrine\Types;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\GuidType;
use Symfony\Component\Uid\Uuid;

class UuidType extends GuidType
{
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return null !== $value ? Uuid::fromString($value) : null;
    }

    public function getName(): string
    {
        return 'uuid';
    }
}
