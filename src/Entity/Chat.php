<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiProperty;
use App\Repository\ChatRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: ChatRepository::class)]
class Chat
{
    use TimestampableEntity;

    #[ORM\Column(type: 'uuid', unique: true)]
    #[ApiProperty(identifier: true, types: ['string'])]
    private Uuid $uuid;

    #[ORM\Id]
    #[ORM\Column(type: 'integer', unique: true)]
    #[ORM\GeneratedValue]
    private $id;

    #[ORM\OneToMany(mappedBy: 'chat', targetEntity: Messages::class, orphanRemoval: true)]
    private Collection $messages;

    #[ORM\Column(length: 255)]
    #[Assert\Choice(['read', 'unread'])]
    private ?string $status = null;

    #[ORM\Column]
    private ?int $newMessagesCount = null;

    #[ORM\Column]
    private ?bool $pinned = null;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $initiator = null;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $companion = null;

    #[ORM\Column(nullable: true)]
    private ?bool $notificationSent = null;

    #[ORM\ManyToOne(inversedBy: 'messages')]
    private ?Submission $submission = null;

    public function __construct()
    {
        $this->uuid = Uuid::v4();
        $this->messages = new ArrayCollection();
        $this->createdAt = new \DateTimeImmutable();
        $this->updatedAt = new \DateTimeImmutable();
    }

    public function getUuid(): Uuid
    {
        return $this->uuid;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection<int, Messages>
     */
    public function getMessages(): Collection
    {
        return $this->messages;
    }

    public function addMessage(Messages $message): static
    {
        if (!$this->messages->contains($message)) {
            $this->messages->add($message);
            $message->setChat($this);
        }

        return $this;
    }

    public function removeMessage(Messages $message): static
    {
        if ($this->messages->removeElement($message)) {
            // set the owning side to null (unless already changed)
            if ($message->getChat() === $this) {
                $message->setChat(null);
            }
        }

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): static
    {
        $this->status = $status;

        return $this;
    }

    public function getNewMessagesCount(): ?int
    {
        return $this->newMessagesCount;
    }

    public function setNewMessagesCount(int $newMessagesCount): static
    {
        $this->newMessagesCount = $newMessagesCount;

        return $this;
    }

    public function isPinned(): ?bool
    {
        return $this->pinned;
    }

    public function setPinned(bool $pinned): static
    {
        $this->pinned = $pinned;

        return $this;
    }

    public function getInitiator(): ?User
    {
        return $this->initiator;
    }

    public function setInitiator(User $initiator): static
    {
        $this->initiator = $initiator;

        return $this;
    }

    public function getCompanion(): ?User
    {
        return $this->companion;
    }

    public function setCompanion(User $companion): static
    {
        $this->companion = $companion;

        return $this;
    }

    public function isNotificationSent(): ?bool
    {
        return $this->notificationSent;
    }

    public function setNotificationSent(bool $notificationSent): static
    {
        $this->notificationSent = $notificationSent;

        return $this;
    }

    public function getSubmission(): ?Submission
    {
        return $this->submission;
    }

    public function setSubmission(?Submission $submission): static
    {
        $this->submission = $submission;

        return $this;
    }
}
