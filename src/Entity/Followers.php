<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiProperty;
use App\Repository\FollowersRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Uid\Uuid;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

#[ORM\Entity(repositoryClass: FollowersRepository::class)]
#[UniqueEntity(fields: ['user', 'followedUser'], message:"This relationship already exists")]
#[UniqueEntity(fields: ['user', 'followedMagazine'], message:"This relationship already exists")]
class Followers
{
    use TimestampableEntity;

    #[ORM\Column(type: 'uuid')]
    #[ApiProperty(identifier: true, types: ['string'])]
    private Uuid $uuid;

    #[ORM\Id]
    #[ORM\Column(type: 'integer')]
    #[ORM\GeneratedValue]
    private $id;

    #[ORM\Column]
    private ?bool $notificationSent = false;

    #[ORM\ManyToOne(targetEntity: 'User', cascade: ['persist'])]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $user = null;

    #[ORM\ManyToOne(targetEntity: 'User')]
    private ?User $followedUser;

    #[ORM\ManyToOne(targetEntity: 'Magazine')]
    private ?Magazine $followedMagazine;

    public function __construct()
    {
        $this->uuid = Uuid::v4();
        $this->createdAt = new \DateTimeImmutable();
        $this->updatedAt = new \DateTimeImmutable();
    }


    public function getFollowedMagazine(): ?Magazine
    {
        return $this->followedMagazine;
    }

    public function setFollowedMagazine(?Magazine $followedMagazine): void
    {
        $this->followedMagazine = $followedMagazine;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUuid(): Uuid
    {
        return $this->uuid;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(User $user): static
    {
        $this->user = $user;

        return $this;
    }

    public function isNotificationSent(): ?bool
    {
        return $this->notificationSent;
    }

    public function setNotificationSent(bool $notificationSent): static
    {
        $this->notificationSent = $notificationSent;

        return $this;
    }

    public function getFollowedUser(): ?User
    {
        return $this->followedUser;
    }

    public function setFollowedUser(?User $followedUser): void
    {
        $this->followedUser = $followedUser;
    }
}
