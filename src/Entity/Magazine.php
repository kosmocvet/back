<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiProperty;
use App\Repository\MagazineRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Magazine entity class.
 */
#[ORM\Entity(repositoryClass: MagazineRepository::class)]
#[ORM\Table(name: '`magazine`')]
class Magazine
{
    use TimestampableEntity;

    #[ORM\Column(type: 'uuid', unique: true)]
    #[ApiProperty(identifier: true, types: ['string'])]
    private Uuid $uuid;

    #[ORM\Id]
    #[ORM\Column(type: 'integer', unique: true)]
    #[ORM\GeneratedValue]
    private $id;

    // Should be changed to media 
    #[ORM\OneToOne]
    private ?Media $cover = null;

    // Primary phone number
    #[ORM\Column(length: 255, nullable: true)]
    private ?string $phone_number = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $about = null;

    // Primary Contact Name
    #[ORM\Column(nullable: true)]
    private ?string $primary_contact = null;

    #[ORM\ManyToOne(targetEntity: User::class)]
    #[ORM\JoinColumn(nullable: true)]
    private ?User $owner = null;

    #[ORM\Column(length: 255)]
    #[Assert\Choice(['Exclusive', 'Exclusive-Pro'])]
    private ?string $type = null;

    #[ORM\ManyToMany(targetEntity: User::class)]
    private Collection $editors;

    #[ORM\Column(length: 255)]
    #[Assert\Choice(['published', 'draft'])]
    private ?string $status = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $tiktok = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $name = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $website = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $instagram = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $pinterest = null;

    // Primary Contact Email
    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Assert\Email(
        message: "Email '{{ value }}' value not have a valid email address",
    )]
    private ?string $primaryContactEmail = null;

    #[ORM\OneToMany(mappedBy: 'magazine', targetEntity: TeamMembers::class)]
    private Collection $teamMembers;

    #[ORM\OneToMany(mappedBy: 'magazine', targetEntity: Submission::class)]
    private Collection $submissions;

    #[ORM\OneToOne]
    private ?MagazineGallery $magazineGallery = null;

    #[ORM\OneToOne]
    private ?Media $aboutPicture = null;

    #[ORM\Column(options: ['default' => 0])]
    private ?int $followersCount = 0;

    #[ORM\ManyToMany(targetEntity: MagazineCategory::class)]
    private Collection $magazineCategories;

    #[ORM\OneToMany(mappedBy: 'magazine', targetEntity: Subscription::class)]
    private Collection $subscriptions;

    #[ORM\OneToMany(mappedBy: 'magazine', targetEntity: Photo::class)]
    private Collection $photos;


    #[ORM\Column(options: ['default' => true])]
    private bool $notifyMessages;

    #[ORM\Column(options: ['default' => true])]
    private bool $notifySubscribers;

    #[ORM\Column(options: ['default' => true])]
    private bool $notifyComments;

    public function __construct()
    {
        $this->uuid = Uuid::v4();
        $this->photos = new ArrayCollection();
        $this->editors = new ArrayCollection();
        $this->teamMembers = new ArrayCollection();
        $this->submissions = new ArrayCollection();
        $this->magazineCategories = new ArrayCollection();
        $this->createdAt = new \DateTimeImmutable();
        $this->updatedAt = new \DateTimeImmutable();
        $this->subscriptions = new ArrayCollection();
    }

    public function getFollowersCount(): int
    {
        return $this->followersCount;

    }

    public function setFollowersCountIncrement(): static
    {
        $this->followersCount = $this->followersCount + 1;

        return $this;
    }

    public function setFollowersCountDecrement(): static
    {
        $this->followersCount = $this->followersCount - 1;

        return $this;
    }
    /**
     * @return Collection<int, Subscription>
     */
    public function getPhotos(): Collection
    {
        return $this->photos;
    }

    public function addPhoto(Photo $photo): static
    {
        if (!$this->photos->contains($photo)) {
            $this->photos->add($photo);
            $photo->setMagazine($this);
        }

        return $this;
    }

    public function removePhoto(Photo $photo): static
    {
        if ($this->photos->removeElement($photo)) {
            // set the owning side to null (unless already changed)
            if ($photo->getMagazine() === $this) {
                $photo->setMagazine(null);
            }
        }

        return $this;
    }



    public function getEditors(): Collection
    {
        return $this->editors;
    }

    public function addEditor(User $editor): static
    {
        if (!$this->editors->contains($editor)) {
            $this->editors->add($editor);
        }

        return $this;
    }

    public function removeEditor(User $editor): static
    {
        $this->editors->removeElement($editor);

        return $this;
    }



    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @return $this
     */
    public function setName(?string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getPinterest(): ?string
    {
        return $this->pinterest;
    }

    /**
     * @return $this
     */
    public function setPinterest(?string $pinterest): static
    {
        $this->pinterest = $pinterest;

        return $this;
    }

    public function getInstagram(): ?string
    {
        return $this->instagram;
    }

    /**
     * @return $this
     */
    public function setInstagram(?string $instagram): static
    {
        $this->instagram = $instagram;

        return $this;
    }

    public function getTiktok(): ?string
    {
        return $this->tiktok;
    }

    /**
     * @return $this
     */
    public function setTiktok(?string $tiktok): static
    {
        $this->tiktok = $tiktok;

        return $this;
    }

    public function getWebsite(): ?string
    {
        return $this->website;
    }

    /**
     * @return $this
     */
    public function setWebsite(?string $website): static
    {
        $this->website = $website;

        return $this;
    }

    public function getPrimaryContactEmail(): ?string
    {
        return $this->primaryContactEmail;
    }

    /**
     * @return $this
     */
    public function setPrimaryContactEmail(?string $primaryContactEmail): static
    {
        $this->primaryContactEmail = $primaryContactEmail;

        return $this;
    }

    public function getUuid(): Uuid
    {
        return $this->uuid;
    }

    public function getId()
    {
        return $this->id;
    }


    public function getCover(): ?Media
    {
        return $this->cover;
    }

    public function setCover(?Media $cover): static
    {
        $this->cover = $cover;

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phone_number;
    }

    /**
     * @return $this
     */
    public function setPhoneNumber(?string $phone_number): static
    {
        $this->phone_number = $phone_number;

        return $this;
    }

    public function getAbout(): ?string
    {
        return $this->about;
    }

    /**
     * @return $this
     */
    public function setAbout(?string $about): static
    {
        $this->about = $about;

        return $this;
    }

    public function getPrimaryContact(): ?string
    {
        return $this->primary_contact;
    }

    /**
     * @return $this
     */
    public function setPrimaryContact(?string $primary_contact): static
    {
        $this->primary_contact = $primary_contact;

        return $this;
    }

    public function getOwner(): ?User
    {
        return $this->owner;
    }

    /**
     * @return $this
     */
    public function setOwner(?User $owner): static
    {
        $this->owner = $owner;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): static
    {
        $this->type = $type;

        return $this;
    }


    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): static
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return Collection<int, TeamMembers>
     */
    public function getTeamMembers(): Collection
    {
        return $this->teamMembers;
    }

    public function addTeamMember(TeamMembers $teamMember): static
    {
        if (!$this->teamMembers->contains($teamMember)) {
            $this->teamMembers->add($teamMember);
            $teamMember->setMagazine($this);
        }

        return $this;
    }

    public function removeTeamMember(TeamMembers $teamMember): static
    {
        if ($this->teamMembers->removeElement($teamMember)) {
            // set the owning side to null (unless already changed)
            if ($teamMember->getMagazine() === $this) {
                $teamMember->setMagazine(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Submission>
     */
    public function getSubmissions(): Collection
    {
        return $this->submissions;
    }

    public function addSubmission(Submission $submission): static
    {
        if (!$this->submissions->contains($submission)) {
            $this->submissions->add($submission);
            $submission->setMagazine($this);
        }

        return $this;
    }

    public function removeSubmission(Submission $submission): static
    {
        if ($this->submissions->removeElement($submission)) {
            // set the owning side to null (unless already changed)
            if ($submission->getMagazine() === $this) {
                $submission->setMagazine(null);
            }
        }

        return $this;
    }

    public function getMagazineGallery(): ?MagazineGallery
    {
        return $this->magazineGallery;
    }

    public function setMagazineGallery(MagazineGallery $magazineGallery): static
    {
        // set the owning side of the relation if necessary
        if ($magazineGallery->getMagazine() !== $this) {
            $magazineGallery->setMagazine($this);
        }

        $this->magazineGallery = $magazineGallery;

        return $this;
    }

    public function getAboutPicture(): ?Media
    {
        return $this->aboutPicture;
    }

    public function setAboutPicture(?Media $aboutPicture): static
    {
        $this->aboutPicture = $aboutPicture;

        return $this;
    }

    /**
     * @return Collection<int, MagazineCategory>
     */
    public function getMagazineCategories(): Collection
    {
        return $this->magazineCategories;
    }

    public function addMagazineCategory(MagazineCategory $magazineCategory): static
    {
        if (!$this->magazineCategories->contains($magazineCategory)) {
            $this->magazineCategories->add($magazineCategory);
        }

        return $this;
    }

    public function removeMagazineCategory(MagazineCategory $magazineCategory): static
    {
        $this->magazineCategories->removeElement($magazineCategory);

        return $this;
    }

    /**
     * @return Collection<int, Subscription>
     */
    public function getSubscriptions(): Collection
    {
        return $this->subscriptions;
    }

    public function addSubscription(Subscription $subscription): static
    {
        if (!$this->subscriptions->contains($subscription)) {
            $this->subscriptions->add($subscription);
            $subscription->setMagazine($this);
        }

        return $this;
    }

    public function removeSubscription(Subscription $subscription): static
    {
        if ($this->subscriptions->removeElement($subscription)) {
            // set the owning side to null (unless already changed)
            if ($subscription->getMagazine() === $this) {
                $subscription->setMagazine(null);
            }
        }

        return $this;
    }

    /**
     * Get the value of notifyMessages
     */ 
    public function getNotifyMessages() : bool
    {
        return $this->notifyMessages;
    }

    /**
     * Set the value of notifyMessages
     *
     * @return  self
     */ 
    public function setNotifyMessages(bool $notifyMessages)
    {
        $this->notifyMessages = $notifyMessages;

        return $this;
    }

    /**
     * Get the value of notifySubscribers
     */ 
    public function getNotifySubscribers():bool
    {
        return $this->notifySubscribers;
    }

    /**
     * Set the value of notifySubscribers
     *
     * @return  self
     */ 
    public function setNotifySubscribers(bool $notifySubscribers)
    {
        $this->notifySubscribers = $notifySubscribers;

        return $this;
    }

    /**
     * Get the value of notifyComments
     */ 
    public function getNotifyComments():bool
    {
        return $this->notifyComments;
    }

    /**
     * Set the value of notifyComments
     *
     * @return  self
     */ 
    public function setNotifyComments(bool $notifyComments)
    {
        $this->notifyComments = $notifyComments;

        return $this;
    }
}
