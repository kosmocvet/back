<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiProperty;
use App\Repository\MagazineGalleryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Uid\Uuid;

#[ORM\Entity(repositoryClass: MagazineGalleryRepository::class)]
class MagazineGallery
{
    use TimestampableEntity;

    #[ORM\Column(type: 'uuid', unique: true)]
    #[ApiProperty(identifier: true, types: ['string'])]
    private Uuid $uuid;

    #[ORM\Id]
    #[ORM\Column(type: 'integer', unique: true)]
    #[ORM\GeneratedValue]
    private $id;

    #[ORM\OneToOne(inversedBy: 'magazineGallery', cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: false)]
    private ?Magazine $magazine = null;

    #[ORM\OneToMany(mappedBy: 'magazineGallery', targetEntity: Photo::class)]
    private Collection $photo;

    public function __construct()
    {
        $this->uuid = Uuid::v4();
        $this->photo = new ArrayCollection();
        $this->createdAt = new \DateTimeImmutable();
        $this->updatedAt = new \DateTimeImmutable();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUuid(): Uuid
    {
        return $this->uuid;
    }

    public function getMagazine(): ?Magazine
    {
        return $this->magazine;
    }

    public function setMagazine(Magazine $magazine): static
    {
        $this->magazine = $magazine;

        return $this;
    }

    /**
     * @return Collection<int, Photo>
     */
    public function getPhoto(): Collection
    {
        return $this->photo;
    }

    public function addPhoto(Photo $photo): static
    {
        if (!$this->photo->contains($photo)) {
            $this->photo->add($photo);
            $photo->setMagazineGallery($this);
        }

        return $this;
    }

    public function removePhoto(Photo $photo): static
    {
        if ($this->photo->removeElement($photo)) {
            // set the owning side to null (unless already changed)
            if ($photo->getMagazineGallery() === $this) {
                $photo->setMagazineGallery(null);
            }
        }

        return $this;
    }
}
