<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Metadata\ApiProperty;
use App\Repository\MediaRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Photo entity class.
 */
#[Vich\Uploadable]
#[ORM\Entity(repositoryClass: MediaRepository::class)]
#[ORM\Table(name: '`media`')]
class Media
{
    use TimestampableEntity;

    #[ORM\Column(type: 'uuid', unique: true)]
    #[ApiProperty(identifier: true, types: ['string'])]
    private Uuid $uuid;

    #[ORM\Id]
    #[ORM\Column(type: 'integer', unique: true)]
    #[ORM\GeneratedValue]
    private string $id;

    #[ORM\Column(nullable: true)]
    private ?string $filePath = null;

    #[Vich\UploadableField(mapping: 'photo', fileNameProperty: 'filePath')]
    #[Assert\NotNull(groups: ['media:create'])]
    #[Assert\File(
        maxSize: '20480K',
        mimeTypes: [
            'image/jpeg',
            'image/png',
        ],
        groups: ['Media:create']
    )]
    public ?File $file = null;

    private ?string $imageUrl;

    #[ORM\ManyToOne(inversedBy: 'attachments', cascade: ['persist', 'remove'])]
    private ?Messages $messages = null;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->uuid = Uuid::v4();
        $this->createdAt = new \DateTimeImmutable();
        $this->updatedAt = new \DateTimeImmutable();
    }

    public function getUuid(): Uuid
    {
        return $this->uuid;
    }

    public function getId(): string
    {
        return $this->id;
    }

    #[Groups(['media:read', 'media:write', 'magazineCategory:read'])]
    public function getImageUrl(): ?string
    {
        return $this->imageUrl = '/media/'.$this->filePath;
    }

    public function getFilePath(): ?string
    {
        return $this->filePath;
    }

    /**
     * @return $this
     */
    public function setFilePath(string $filePath): static
    {
        $this->filePath = $filePath;

        return $this;
    }

    public function getMessages(): ?Messages
    {
        return $this->messages;
    }

    public function setMessages(?Messages $messages): static
    {
        $this->messages = $messages;

        return $this;
    }
}
