<?php

declare(strict_types=1);

namespace App\Entity;


use ApiPlatform\Metadata\ApiProperty;
use App\Repository\PhotoRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Photo entity class.
 */


#[Vich\Uploadable]
#[ORM\Entity(repositoryClass: PhotoRepository::class)]
#[ORM\Table(name: '`photo`')]
class Photo
{
    use TimestampableEntity;

    #[ORM\Column(type: 'uuid', unique: true)]
    #[ApiProperty(identifier: true, types: ['string'])]
    private Uuid $uuid;

    #[ORM\Id]
    #[ORM\Column(type: 'integer', unique: true)]
    #[ORM\GeneratedValue]
    private string $id;

    public string $imageUrl;

    #[Assert\NotNull()]
    #[Assert\NotBlank]
    #[Assert\Length(max: 255)]
    #[ORM\Column(length: 255)]
    private ?string $caption = null;

    #[Assert\NotNull()]
    #[ORM\Column(type: 'boolean', options: ['default' => 0])]
    private ?bool $isSensitiveContent = null;

    #[Assert\NotNull]
    #[Assert\Valid]
    #[Assert\Type(type: User::class)]
    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'photos')]
    #[ORM\JoinColumn(name: 'user_id', referencedColumnName: 'id', nullable: false)]
    private ?User $user = null;

    #[Assert\NotNull]
    #[Assert\Type(type: Category::class)]
    #[ORM\ManyToOne(targetEntity: Category::class, cascade: ['persist'])]
    #[ORM\JoinColumn(name: 'category_id', referencedColumnName: 'id', nullable: false)]
    #[ORM\OrderBy(['createdAt', 'DESC'])]
    private ?Category $category = null;

    #[ORM\Column(type: 'integer', nullable: true)]
    public ?int $likes = null;

    #[ORM\ManyToOne(targetEntity: Magazine::class, inversedBy: 'photos')]
    private ?Magazine $magazine = null;

    #[ORM\Column(nullable: true)]
    public ?\DateTimeImmutable $likesLastDate = null;

    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: false)]
    private ?Media $media = null;

    #[ORM\ManyToOne(inversedBy: 'photo')]
    private ?MagazineGallery $magazineGallery = null;

    #[ORM\Column]
    private ?int $commentsCount = 0;

    #[ORM\OneToMany(mappedBy: 'photo', targetEntity: Comment::class)]
    private Collection $comments;

    #[ORM\OneToMany(mappedBy: 'photo', targetEntity: CreditName::class)]
    private Collection $creators;

    public function __toString(): string
    {
        return $this->caption;
    }

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->uuid = Uuid::v4();
        $this->createdAt = new \DateTimeImmutable();
        $this->updatedAt = new \DateTimeImmutable();
        $this->comments = new ArrayCollection();
        $this->creators = new ArrayCollection();
    }

    public function getMagazine(): ?Magazine
    {
        return $this->magazine;
    }

    /**
     * @return ?$this
     */
    public function setMagazine(?Magazine $magazine): ?static
    {
        $this->magazine = $magazine;

        return $this;
    }

    public function getCreators(): Collection
    {
        return $this->creators;
    }

    public function addCreator(CreditName $creator): static
    {
        if (!$this->creators->contains($creator)) {
            $this->creators->add($creator);
        }

        return $this;
    }

    public function removeCreator(CreditName $creator): static
    {
        $this->creators->removeElement($creator);

        return $this;
    }

    public function getUuid(): Uuid
    {
        return $this->uuid;
    }

    public function getId(): string
    {
        return $this->id;
    }


    public function setCommentsCountIncrement(): static
    {
        $this->commentsCount = $this->commentsCount + 1;

        return $this;
    }

    public function setCommentsCountDecrement(): static
    {
        $this->commentsCount = $this->commentsCount - 1;

        return $this;
    }

    public function getCaption(): ?string
    {
        return $this->caption;
    }

    /**
     * @return $this
     */
    public function setCaption(string $caption): static
    {
        $this->caption = $caption;

        return $this;
    }

    public function getIsSensitiveContent(): ?bool
    {
        return $this->isSensitiveContent;
    }

    /**
     * @return $this
     */
    public function setIsSensitiveContent(bool $isSensitiveContent): static
    {
        $this->isSensitiveContent = $isSensitiveContent;

        return $this;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return $this
     */
    public function setUser(?User $user): static
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Category|null
     */
    public function getCategory(): Category
    {
        return $this->category;
    }

    /**
     * @return ?$this
     */
    public function setCategory(?Category $category): ?static
    {
        $this->category = $category;

        return $this;
    }

    public function getFilePath(): ?string
    {
        return $this->filePath;
    }

    /**
     * @return $this
     */
    public function setFilePath(string $filePath): static
    {
        $this->filePath = $filePath;

        return $this;
    }

    public function getLikes(): ?int
    {
        return $this->likes;
    }

    public function setLikes(?int $likes): static
    {
        $this->likes = $likes;

        return $this;
    }

    public function getLikesLastDate(): ?\DateTimeImmutable
    {
        return $this->likesLastDate;
    }

    public function setLikesLastDate(?\DateTimeImmutable $likesLastDate): void
    {
        $this->likesLastDate = $likesLastDate;
    }

    public function getMedia(): ?Media
    {
        return $this->media;
    }

    public function setMedia(Media $media): static
    {
        $this->media = $media;

        return $this;
    }

    public function getMagazineGallery(): ?MagazineGallery
    {
        return $this->magazineGallery;
    }

    public function setMagazineGallery(?MagazineGallery $magazineGallery): static
    {
        $this->magazineGallery = $magazineGallery;

        return $this;
    }

    public function getCommentsCount(): ?int
    {
        return $this->commentsCount;
    }

    public function setCommentsCount(int $commentsCount): static
    {
        $this->commentsCount = $commentsCount;

        return $this;
    }

    public function getImageUrl(): ?string
    {
        return $this->imageUrl = $this->media->getImageUrl();
    }

    /**
     * @return Collection<int, Comment>
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): static
    {
        if (!$this->comments->contains($comment)) {
            $this->comments->add($comment);
            $comment->setPhoto($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): static
    {
        if ($this->comments->removeElement($comment)) {
            // set the owning side to null (unless already changed)
            if ($comment->getPhoto() === $this) {
                $comment->setPhoto(null);
            }
        }

        return $this;
    }
}
