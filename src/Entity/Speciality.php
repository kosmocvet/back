<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiProperty;
use App\Repository\SpecialityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Speciality entity class.
 */
#[ORM\Entity(repositoryClass: SpecialityRepository::class)]
#[ORM\Table(name: '`speciality`')]
#[UniqueEntity(fields: 'caption', message: 'Speciality with caption {{ value }} already exists')]
class Speciality
{
    use TimestampableEntity;

    #[ORM\Column(type: 'uuid', unique: true)]
    #[ApiProperty(identifier: true)]
    private Uuid $uuid;

    #[ORM\Id]
    #[ORM\Column(type: 'integer', unique: true)]
    #[ORM\GeneratedValue]
    private string $id;

    #[ORM\Column(length: 255)]
    private ?string $caption = null;

    #[ORM\ManyToMany(targetEntity: User::class, mappedBy: 'specialities')]
    #[Assert\Valid]
    private Collection|ArrayCollection $users;

    public function __toString(): string
    {
        return $this->caption;
    }

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->uuid = Uuid::v4();
        $this->users = new ArrayCollection();
        $this->createdAt = new \DateTimeImmutable();
        $this->updatedAt = new \DateTimeImmutable();
    }

    public function getUuid(): ?Uuid
    {
        return $this->uuid;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getCaption(): ?string
    {
        return $this->caption;
    }

    /**
     * @return $this
     */
    public function setCaption(string $caption): static
    {
        $this->caption = $caption;

        return $this;
    }

    public function getUsers(): Collection
    {
        return $this->users;
    }

    /**
     * @return $this
     */
    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users->add($user);
            $user->addSpeciality($this);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeUser(User $user): self
    {
        if ($this->users->removeElement($user)) {
            $user->removeSpeciality($this);
        }

        return $this;
    }
}
