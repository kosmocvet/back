<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiProperty;
use App\Repository\SubmissionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: SubmissionRepository::class)]
class Submission
{
    use TimestampableEntity;

    #[ORM\Column(type: 'uuid', unique: true)]
    #[ApiProperty(identifier: true, types: ['string'])]
    private Uuid $uuid;

    #[ORM\Id]
    #[ORM\Column(type: 'integer', unique: true)]
    #[ORM\GeneratedValue]
    private $id;

    #[ORM\Column(length: 255)]
    #[Assert\Choice(['open', 'draft', 'in progress', 'closed'])]
    private ?string $status = null;

    #[ORM\Column(type: Types::DECIMAL, precision: 10, scale: 2)]
    private ?string $startPrice = null;

    #[ORM\Column(type: Types::DECIMAL, precision: 10, scale: 2, nullable: true)]
    private ?string $endPrice = null;

    #[ORM\Column(length: 255)]
    #[Assert\Choice(['USD', 'EUR'])]
    private ?string $currency = null;

    #[ORM\Column(length: 255)]
    #[Assert\Choice(['standard response', 'exclusive response'])]
    private ?string $type = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\Column(type: Types::TEXT)]
    private ?string $description = null;
    
    #[ORM\ManyToOne(inversedBy: 'submissions')]
    private ?Magazine $magazine = null;

    #[ORM\OneToMany(mappedBy: 'submission', targetEntity: Chat::class)]
    private Collection $messages;

    public function __construct()
    {
        $this->uuid = Uuid::v4();
        $this->messages = new ArrayCollection();
        $this->createdAt = new \DateTimeImmutable();
        $this->updatedAt = new \DateTimeImmutable();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUuid(): Uuid
    {
        return $this->uuid;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): static
    {
        $this->status = $status;

        return $this;
    }

    public function getStartPrice(): ?string
    {
        return $this->startPrice;
    }

    public function setStartPrice(string $startPrice): static
    {
        $this->startPrice = $startPrice;

        return $this;
    }

    public function getEndPrice(): ?string
    {
        return $this->endPrice;
    }

    public function setEndPrice(?string $endPrice): static
    {
        $this->endPrice = $endPrice;

        return $this;
    }

    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    public function setCurrency(string $currency): static
    {
        $this->currency = $currency;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): static
    {
        $this->type = $type;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): static
    {
        $this->description = $description;

        return $this;
    }

    public function getMagazine(): ?Magazine
    {
        return $this->magazine;
    }

    public function setMagazine(?Magazine $magazine): static
    {
        $this->magazine = $magazine;

        return $this;
    }

    public function getMessages() : Collection
    {
        return $this->messages;
    }

    public function addMessage(Chat $message): static
    {
        if (!$this->messages->contains($message)) {
            $this->messages->add($message);
            $message->setSubmission($this);
        }

        return $this;
    }

    public function removeMessage(Chat $message): static
    {
        if ($this->messages->removeElement($message)) {
            // set the owning side to null (unless already changed)
            if ($message->getSubmission() === $this) {
                $message->setSubmission(null);
            }
        }

        return $this;
    }
}

