<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiProperty;
use App\Enum\UserRole;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * User entity class.
 */
#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ORM\Table(name: '`user`')]
#[UniqueEntity(fields: 'email', message: 'Email {{ value }} already exists')]
#[ORM\HasLifecycleCallbacks]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    use TimestampableEntity;

    #[ORM\Column(type: 'uuid', unique: true)]
    #[ApiProperty(identifier: true, types: ['string'])]
    private Uuid $uuid;

    #[ORM\Id]
    #[ORM\Column(type: 'integer', unique: true)]
    #[ORM\GeneratedValue]
    private string $id;

    #[Assert\NotNull]
    #[Assert\NotBlank]
    #[Assert\Email(
        message: "Email '{{ value }}' value not have a valid email address",
    )]
    #[Assert\Length(max: 100)]
    #[ORM\Column(length: 180, unique: true)]
    private ?string $email = null;

    #[ORM\Column(length: 255)]
    private ?string $accountEmail = null;

    #[ORM\Column]
    private array $roles = [];
    /**
     * @var string|null The hashed password
     */
    #[ORM\Column]
    private ?string $password = null;

    #[SerializedName('password')]
    private ?string $plainPassword = null;

    #[Assert\NotNull()]
    #[Assert\NotBlank]
    #[Assert\Length(max: '255')]
    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[Assert\Length(max: '255')]
    #[ORM\Column(length: 255)]
    private ?string $location = null;

    #[Assert\Valid]
    #[ORM\ManyToMany(targetEntity: Speciality::class, inversedBy: 'users', cascade: ['persist'])]
    private Collection $specialities;

    #[ORM\Column(type: Types::TEXT)]
    private ?string $about = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $website = null;

    #[Assert\Length(max: '255')]
    #[ORM\Column(length: 255, nullable: true)]
    private ?string $instagram = null;

    #[Assert\Length(max: '255')]
    #[ORM\Column(length: 255, nullable: true)]
    private ?string $tiktok = null;

    #[Assert\Length(max: '255')]
    #[ORM\Column(length: 255, nullable: true)]
    private ?string $pinterest = null;

    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: true)]
    private ?Media $avatar = null;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: Magazine::class, cascade: ['persist', 'remove'])]
    private Collection $magazine;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: Photo::class, cascade: ['persist', 'remove'])]
    private Collection|ArrayCollection $photos;

    #[ORM\ManyToMany(targetEntity: Magazine::class)]
    private Collection $editMagazine;

    #[ORM\ManyToOne(inversedBy: 'user')]
    private ?TeamMembers $teamMembers = null;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: Comment::class)]
    private Collection $comments;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: CreditName::class)]
    private Collection $credits;

    #[ORM\Column(options: ['default' => 0])]
    private ?int $followersCount = 0;

    #[ORM\Column(options: ['default' => 0])]
    private ?int $followedCount = 0;

    #[ORM\Column(length: 255)]
    #[Assert\Choice(['online', 'offline'])]
    private ?string $status = null;

    #[ORM\OneToOne(mappedBy: 'user', cascade: ['persist', 'remove'])]
    private ?Notification $notification = null;

    private string $userPage;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: Subscription::class)]
    private Collection $subscriptions;

    #[ORM\Column(options: ['default' => true])]
    private bool $notifyMessages;

    #[ORM\Column(options: ['default' => true])]
    private bool $notifySubscribers;

    public function __toString(): string
    {
        return $this->name;
    }

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->credits = new ArrayCollection();
        $this->editMagazine = new ArrayCollection();
        $this->uuid = Uuid::v4();
        $this->roles[] = 'ROLE_USER';
        $this->photos = new ArrayCollection();
        $this->specialities = new ArrayCollection();
        $this->comments = new ArrayCollection();
        $this->createdAt = new \DateTimeImmutable();
        $this->updatedAt = new \DateTimeImmutable();
        $this->status = 'offline';
        $this->subscriptions = new ArrayCollection();
    }



    public function getСredits(): Collection
    {
        return $this->credits;
    }

    public function addСredit(CreditName $credit): static
    {
        if (!$this->credits->contains($credit)) {
            $this->credits->add($credit);
            $credit->setUser($this);
        }

        return $this;
    }

    public function removeСredit(CreditName $credit): static
    {
        if ($this->credits->removeElement($credit)) {
            // set the owning side to null (unless already changed)
            if ($credit->getUser() === $this) {
                $credit->getUser(null);
            }
        }

        return $this;
    }




    public function getMagazines(): Collection
    {
        return $this->magazine;
    }

    public function addMagazine(Magazine $magazine): static
    {
        if (!$this->magazine->contains($magazine)) {
            $this->magazine->add($magazine);
            $magazine->setOwner($this);
        }

        return $this;
    }

    public function removeMagazine(Magazine $magazine): static
    {
        if ($this->comments->removeElement($magazine)) {
            // set the owning side to null (unless already changed)
            if ($magazine->getOwner() === $this) {
                $magazine->setOwner(null);
            }
        }

        return $this;
    }



    public function getEditMagazine(): Collection
    {
        return $this->editMagazine;
    }

    /**
     * @return $this
     */
    public function addEditMagazine(Magazine $magazine): static
    {
        if (!$this->editMagazine->contains($magazine)) {
            $this->editMagazine->add($magazine);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeEditMagazine(Magazine $magazine): static
    {
        $this->editMagazine->removeElement($magazine);

        return $this;
    }





    public function getUserPage(): string
    {
        return '/account/' . $this->uuid;
    }

    public function getUuid(): ?Uuid
    {
        return $this->uuid;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @return $this
     */
    public function setEmail(string $email): static
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        $roles[] = UserRole::Visitor->value;

        return array_unique($roles);
    }

    /**
     * @return $this
     */
    public function setRoles(array $roles): static
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @return $this
     */
    public function setPassword(string $password): static
    {
        $this->password = $password;

        return $this;
    }

    public function getPlainPassword(): string
    {
        return $this->plainPassword;
    }

    /**
     * @param mixed $plainPassword
     */
    public function setPlainPassword(string $plainPassword): static
    {
        $this->plainPassword = $plainPassword;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        $this->plainPassword = null;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @return $this
     */
    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getLocation(): ?string
    {
        return $this->location;
    }

    /**
     * @return $this
     */
    public function setLocation(string $location): static
    {
        $this->location = $location;

        return $this;
    }

    /**
     * @return Collection<int, Speciality>
     */
    public function getSpecialities(): Collection
    {
        return $this->specialities;
    }

    /**
     * @return $this
     */
    public function addSpeciality(Speciality $speciality): static
    {
        if (!$this->specialities->contains($speciality)) {
            $this->specialities->add($speciality);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeSpeciality(Speciality $speciality): static
    {
        $this->specialities->removeElement($speciality);

        return $this;
    }

    public function getWebsite(): ?string
    {
        return $this->website;
    }

    /**
     * @return $this
     */
    public function setWebsite(?string $website): static
    {
        $this->website = $website;

        return $this;
    }

    public function getInstagram(): ?string
    {
        return $this->instagram;
    }

    /**
     * @return $this
     */
    public function setInstagram(?string $instagram): static
    {
        $this->instagram = $instagram;

        return $this;
    }

    public function getTiktok(): ?string
    {
        return $this->tiktok;
    }

    /**
     * @return $this
     */
    public function setTiktok(?string $tiktok): static
    {
        $this->tiktok = $tiktok;

        return $this;
    }

    public function getPinterest(): ?string
    {
        return $this->pinterest;
    }

    /**
     * @return $this
     */
    public function setPinterest(?string $pinterest): static
    {
        $this->pinterest = $pinterest;

        return $this;
    }

    public function getAvatar(): ?Media
    {
        return $this->avatar;
    }

    public function setAvatar(?Media $avatar): void
    {
        $this->avatar = $avatar;
    }

    public function getAbout(): ?string
    {
        return $this->about;
    }

    /**
     * @return $this
     */
    public function setAbout(string $about): static
    {
        $this->about = $about;

        return $this;
    }

    /**
     * @return Collection<int, Photo>
     */
    public function getPhotos(): Collection
    {
        return $this->photos;
    }

    /**
     * @return $this
     */
    public function addPhoto(Photo $photo): static
    {
        if (!$this->photos->contains($photo)) {
            $this->photos->add($photo);
            $photo->setUser($this);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removePhoto(Photo $photo): static
    {
        if ($this->photos->removeElement($photo)) {
            // set the owning side to null (unless already changed)
            if ($photo->getUser() === $this) {
                $photo->setUser(null);
            }
        }

        return $this;
    }

    public function setAccountEmail(?string $accountEmail): static
    {
        $this->accountEmail = $accountEmail;

        return $this;
    }

    public function getAccountEmail(): ?string
    {
        return $this->accountEmail;
    }


    public function getTeamMembers(): ?TeamMembers
    {
        return $this->teamMembers;
    }

    public function setTeamMembers(?TeamMembers $teamMembers): static
    {
        $this->teamMembers = $teamMembers;

        return $this;
    }

    /**
     * @return Collection<int, Comment>
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): static
    {
        if (!$this->comments->contains($comment)) {
            $this->comments->add($comment);
            $comment->setUser($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): static
    {
        if ($this->comments->removeElement($comment)) {
            // set the owning side to null (unless already changed)
            if ($comment->getUser() === $this) {
                $comment->setUser(null);
            }
        }

        return $this;
    }

    public function getFollowersCount(): ?int
    {
        return $this->followersCount;
    }

    public function setFollowersCount(int $followersCount): static
    {
        $this->followersCount = $followersCount;

        return $this;
    }


    public function setFollowedCountIncrement(): static
    {
        $this->followedCount = $this->followedCount + 1;

        return $this;
    }

    public function setFollowedCountDecrement(): static
    {
        $this->followedCount = $this->followedCount - 1;

        return $this;
    }


    public function setFollowersCountIncrement(): static
    {
        $this->followersCount = $this->followersCount + 1;

        return $this;
    }

    public function setFollowersCountDecrement(): static
    {
        $this->followersCount = $this->followersCount - 1;

        return $this;
    }

    public function getFollowedCount(): ?int
    {
        return $this->followedCount;
    }

    public function setFollowedCount(int $followedCount): static
    {
        $this->followedCount = $followedCount;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): static
    {
        $this->status = $status;

        return $this;
    }

    public function getNotification(): ?Notification
    {
        return $this->notification;
    }

    public function setNotification(Notification $notification): static
    {
        // set the owning side of the relation if necessary
        if ($notification->getUser() !== $this) {
            $notification->setUser($this);
        }

        $this->notification = $notification;

        return $this;
    }

    public function getChat(): ?Chat
    {
        return $this->chat;
    }

    public function setChat(Chat $chat): static
    {
        // set the owning side of the relation if necessary
        if ($chat->getInitiator() !== $this) {
            $chat->setInitiator($this);
        }

        $this->chat = $chat;

        return $this;
    }

    /**
     * @return Collection<int, Subscription>
     */
    public function getSubscriptions(): Collection
    {
        return $this->subscriptions;
    }

    public function addSubscription(Subscription $subscription): static
    {
        if (!$this->subscriptions->contains($subscription)) {
            $this->subscriptions->add($subscription);
            $subscription->setUser($this);
        }

        return $this;
    }

    public function removeSubscription(Subscription $subscription): static
    {
        if ($this->subscriptions->removeElement($subscription)) {
            // set the owning side to null (unless already changed)
            if ($subscription->getUser() === $this) {
                $subscription->setUser(null);
            }
        }

        return $this;
    }

    /**
     * Get the value of notifyMessages
     */ 
    public function getNotifyMessages():bool
    {
        return $this->notifyMessages;
    }

    /**
     * Set the value of notifyMessages
     *
     * @return  self
     */ 
    public function setNotifyMessages(bool $notifyMessages)
    {
        $this->notifyMessages = $notifyMessages;

        return $this;
    }

    /**
     * Get the value of notifySubscribers
     */ 
    public function getNotifySubscribers() :bool
    {
        return $this->notifySubscribers;
    }

    /**
     * Set the value of notifySubscribers
     *
     * @return  self
     */ 
    public function setNotifySubscribers(bool $notifySubscribers)
    {
        $this->notifySubscribers = $notifySubscribers;

        return $this;
    }
}
