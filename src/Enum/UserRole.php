<?php

namespace App\Enum;

enum UserRole: string
{
    case Visitor = 'ROLE_USER';
    case Regular = 'ROLE_REGULAR_USER';
    case MagazineOwner = 'ROLE_MAGAZINE_OWNER';
    case MagazineEditor = 'ROLE_MAGAZINE_EDITOR';
    case SuperAdmin = 'ROLE_SUPER_ADMIN';
}
