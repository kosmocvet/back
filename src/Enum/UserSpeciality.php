<?php

namespace App\Enum;

enum UserSpeciality: string
{
    case Photographer = 'Photographer';
    case Model = 'Model';
    case MakeupArtist = 'Makeup Artist';
    case HairStylist = 'Hair Stylist';
    case FashionDesigner = 'Fashion Designer';
    case Retoucher = 'Retoucher';
    case Editor = 'Editor';
    case CreativeDirector = 'Creative Director';
}
