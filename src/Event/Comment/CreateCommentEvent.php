<?php

namespace App\Event\Comment;

use Symfony\Contracts\EventDispatcher\Event;

class CreateCommentEvent extends Event
{
    protected $photo;
    protected $entityManager;

    public function __construct($photo,$entityManager)
    {
        $this->photo = $photo;
        $this->entityManager = $entityManager;
    }

    public function getPhoto()
    {
        return $this->photo;
    }

    public function getEntityManager()
    {
        return $this->entityManager;
    }
}