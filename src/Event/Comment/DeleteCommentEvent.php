<?php

namespace App\Event\Comment;

use Symfony\Contracts\EventDispatcher\Event;

class DeleteCommentEvent extends Event
{
    protected $user;

    public function __construct($user)
    {
        $this->user = $user;
    }

    public function getUser()
    {
        return $this->user;
    }
}