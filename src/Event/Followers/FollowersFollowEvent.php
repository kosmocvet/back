<?php

namespace App\Event\Followers;

use App\Entity\Magazine;
use Symfony\Contracts\EventDispatcher\Event;
use App\Entity\User;
use Doctrine\ORM\EntityManager;

class FollowersFollowEvent extends Event
{
    protected $follower;
    protected $followed;
    protected $entityManager;

    public function __construct(User $follower,EntityManager $entityManager,User|Magazine $followed)
    {
        $this->follower = $follower;
        $this->followed = $followed;
        $this->entityManager = $entityManager;
    }

    public function getFollower()
    {
        return $this->follower;
    }

    public function getFollowed()
    {
        return $this->followed;
    }

    public function getEntityManager()
    {
        return $this->entityManager;
    }
}