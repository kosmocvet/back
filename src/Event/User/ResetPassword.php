<?php

namespace App\Event\User;

use App\Entity\User;
use App\Service\EmailSender;
use App\Service\PasswordGeneratorService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Contracts\EventDispatcher\Event;

class ResetPassword extends Event
{
    protected $user;
    protected $em;
    protected $hasher;
    protected $passwordGeneratorService;
    protected $emailSender;



    public function __construct(User $user,
    EntityManagerInterface $em,
    UserPasswordHasherInterface $hasher,
    PasswordGeneratorService $passwordGeneratorService,
    EmailSender $emailSender)
    {
        $this->user = $user;
        $this->hasher = $hasher;
        $this->passwordGeneratorService = $passwordGeneratorService;
        $this->em = $em;
        $this->emailSender = $emailSender;
    }

    public function getEmailSender()
    {
        return $this->emailSender;
    }

    public function getPasswordGeneratorService()
    {
        return $this->passwordGeneratorService;
    }

    public function getHasher()
    {
        return $this->hasher;
    }


    public function getEm()
    {
        return $this->em;
    }

    public function getUser()
    {
        return $this->user;
    }

}