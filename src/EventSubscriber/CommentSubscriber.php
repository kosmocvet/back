<?php

namespace App\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class CommentSubscriber implements EventSubscriberInterface
{
    public function onCommentCreated($event): void
    {
        $photo = $event->getPhoto();
        $entityManager = $event->getEntityManager();
        $photo->setCommentsCountIncrement();
        $entityManager->persist($photo);
        $entityManager->flush();
    }

    public static function getSubscribedEvents(): array
    {
        return [
            'comment.created' => 'onCommentCreated',
        ];
    }
}
