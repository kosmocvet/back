<?php

namespace App\EventSubscriber;

use App\Entity\Followers;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class FollowersSubscriber implements EventSubscriberInterface
{
    public function onFollowersFollow($event): void
    {
        $follower = $event->getFollower();
        $followed = $event->getFollowed();
        $entityManager = $event->getEntityManager();

        $followed->setFollowersCountIncrement();
        $entityManager->persist($followed);
        $entityManager->flush();

        $follower->setFollowedCountIncrement();
        $entityManager->persist($follower);
        $entityManager->flush();

    }


    public function onFollowersUnFollow($event): void
    {
        $follower = $event->getFollower();
        $followed = $event->getFollowed();
        $entityManager = $event->getEntityManager();


        $followed->setFollowersCountDecrement();
        $entityManager->persist($followed);
        $entityManager->flush();

        $follower->setFollowedCountDecrement();
        $entityManager->persist($follower);
        $entityManager->flush();

    }


    public static function getSubscribedEvents(): array
    {
        return [
            'followers.follow' => 'onFollowersFollow',
            'followers.unfollow' => 'onFollowersUnFollow',
        ];
    }
}
