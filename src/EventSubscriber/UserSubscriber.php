<?php

namespace App\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Mime\Address;

class UserSubscriber implements EventSubscriberInterface
{
    public function onUserReset($event): void
    {

        $emailSender = $event->getEmailSender();
        $passwordGeneratorService = $event->getPasswordGeneratorService();
        $hasher = $event->getHasher();
        $user = $event->getUser();
        $em = $event->getEm();
        
        $password = $passwordGeneratorService->generatePassword();
        $user->setPassword($hasher->hashPassword($user, $password));
        $em->persist($user);
        $em->flush($user);

        $emailSender
        ->to($user->getEmail())
        ->subject('Password Reset - Depo Magazines')
        ->htmlTemplate('email/reset_password.html.twig')
        ->context([
            'name' => $user->getName(),
            'password' => $password,
        ])
        ->send();
    }

    public static function getSubscribedEvents(): array
    {
        return [
            'user.reset' => 'onUserReset',
        ];
    }
}
