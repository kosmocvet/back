<?php

namespace App\Model;

class PageModel
{
    private $pageUrl;

    public function __construct(string $pageUrl)
    {
        $this->pageUrl = $pageUrl;
    }

    public function getPageUrl(): string
    {
        return $this->pageUrl;
    }
}