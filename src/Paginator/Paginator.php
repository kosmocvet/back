<?php

declare(strict_types=1);

namespace App\Paginator;

class Paginator
{

    public function getHydraParody(string $url,int|string|float $perPage,int|string|float $totalItems,int|string|float $page) :array
    {
        
        $view = [];
        $view["@id"] = $url . $page;
        $view["hydra:first"] = $url . 1;
        $view["hydra:last"] = $url . ceil($totalItems / $perPage);
        if($page != 1) $view["hydra:previous"] = $url . $page - 1;
        if($page != ceil($totalItems / $perPage)) $view["hydra:next"] = $url . $page + 1;
        
        return $view;
    }
}
