<?php

namespace App\Repository;

use App\Entity\CreditName;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<CreditName>
 *
 * @method CreditName|null find($id, $lockMode = null, $lockVersion = null)
 * @method CreditName|null findOneBy(array $criteria, array $orderBy = null)
 * @method CreditName[]    findAll()
 * @method CreditName[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CreditNameRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CreditName::class);
    }

//    /**
//     * @return CreditName[] Returns an array of CreditName objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('c.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?CreditName
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
