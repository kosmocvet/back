<?php

namespace App\Repository;

use App\Entity\MagazineCategory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<MagazineCategory>
 *
 * @method MagazineCategory|null find($id, $lockMode = null, $lockVersion = null)
 * @method MagazineCategory|null findOneBy(array $criteria, array $orderBy = null)
 * @method MagazineCategory[]    findAll()
 * @method MagazineCategory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MagazineCategoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MagazineCategory::class);
    }

//    /**
//     * @return MagazineCategory[] Returns an array of MagazineCategory objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('m')
//            ->andWhere('m.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('m.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?MagazineCategory
//    {
//        return $this->createQueryBuilder('m')
//            ->andWhere('m.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
