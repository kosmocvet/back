<?php

namespace App\Repository;

use App\Entity\MagazineGallery;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<MagazineGallery>
 *
 * @method MagazineGallery|null find($id, $lockMode = null, $lockVersion = null)
 * @method MagazineGallery|null findOneBy(array $criteria, array $orderBy = null)
 * @method MagazineGallery[]    findAll()
 * @method MagazineGallery[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MagazineGalleryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MagazineGallery::class);
    }

//    /**
//     * @return MagazineGallery[] Returns an array of MagazineGallery objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('m')
//            ->andWhere('m.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('m.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?MagazineGallery
//    {
//        return $this->createQueryBuilder('m')
//            ->andWhere('m.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
