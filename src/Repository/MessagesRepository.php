<?php

namespace App\Repository;

use App\Entity\Messages;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Messages>
 *
 * @method Messages|null find($id, $lockMode = null, $lockVersion = null)
 * @method Messages|null findOneBy(array $criteria, array $orderBy = null)
 * @method Messages[]    findAll()
 * @method Messages[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MessagesRepository extends ServiceEntityRepository
{

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Messages::class);
    }

    /**
     * @return Messages[] Returns an array of Messages objects
     */
    public function findByChat($chat, $page): array
    {
        $query = $this->getEntityManager()->createQuery("
                SELECT m, a FROM App\Entity\Messages m
                LEFT JOIN m.attachments a
                WHERE m.chat = :chat
                ORDER BY m.createdAt ASC
            ")->setParameter('chat', $chat)
            ->setMaxResults(50);

        $offset = ($page - 1) * 50;

        $query->setFirstResult($offset);

        return $query->getResult();
    }

    //    public function findOneBySomeField($value): ?Messages
    //    {
    //        return $this->createQueryBuilder('m')
    //            ->andWhere('m.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
