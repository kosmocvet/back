<?php

namespace App\Security;

use App\Repository\ApiTokenRepository;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\Exception\UserNotFoundException;
use Symfony\Component\Security\Http\Authenticator\AbstractAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\Authenticator\Passport\SelfValidatingPassport;

class ApiTokenAuthenticator extends AbstractAuthenticator
{

    /**
     * @var UserRepository
     */
    private UserRepository $userRepository;
    private ApiTokenRepository $apiTokenRepository;

    public function __construct(UserRepository $userRepository, ApiTokenRepository $apiTokenRepository)
    {
        $this->userRepository = $userRepository;
        $this->apiTokenRepository = $apiTokenRepository;
    }

    public function supports(Request $request): ?bool
    {
        return str_starts_with($request->getPathInfo(), '/api/');
    }

    public function authenticate(Request $request): Passport
    {
        $bearer = $request->headers->get('Authorization');
        if (null === $bearer) {
            throw new CustomUserMessageAuthenticationException('No API token provided');
        }
        $apiToken = explode(' ', $bearer)[1];

        return new SelfValidatingPassport(
            new UserBadge($apiToken, function ($apiToken) {

                if (null === $apiTokenEntity = $this->apiTokenRepository->findOneBy(['token' => $apiToken])) {
                    throw new UserNotFoundException();
                }

                if ($apiTokenEntity->getExpiredAt() < new \DateTime('now')) {
                    $this->apiTokenRepository->remove($apiTokenEntity);
                    throw new CustomUserMessageAuthenticationException('API token was expired');
                }

                if (null === $user = $apiTokenEntity->getUser()) {
                    throw new UserNotFoundException();
                }
                return $user;
            })
        );
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        return null;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?Response
    {
        $data = [
            'message' => strtr($exception->getMessageKey(), $exception->getMessageData()),
        ];

        return new JsonResponse($data, Response::HTTP_UNAUTHORIZED);
    }
}
