<?php

namespace App\Security;

use App\Entity\ApiToken;
use App\Service\UserTokenService;
use Scheb\TwoFactorBundle\Security\Authentication\Token\TwoFactorTokenInterface;
use Scheb\TwoFactorBundle\Security\TwoFactor\Provider\Totp\TotpAuthenticator;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;

class AuthenticationSuccessHandler implements AuthenticationSuccessHandlerInterface
{
    public function __construct(private UserTokenService $userTokenService)
    {
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token): Response
    {

        $apiToken = $this->userTokenService->getActiveToken();

        return new JsonResponse([
            // Format fix for frontenders
            // 'login'  => 'success',
            // 'userId' => $apiToken->getUser()->getId(),
            'token'  => $apiToken->getToken(),
        ]);
    }
}