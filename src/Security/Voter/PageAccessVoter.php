<?php

namespace App\Security\Voter;

use App\Model\PageModel;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

// ... (інші import-и)

class PageAccessVoter extends Voter
{
    protected $rolesPages = [
        'Visitor' => [
            '/gallery',
            '/account',
            '/authorization',
            '/sign-up',
            '/sign-in-user',
        ],
        'Regular User' => [
            '/gallery',
            '/account',
            '/account/settings',
            '/account/add-photo',
            // ... (інші сторінки)
        ],
    ];

    protected function supports($attribute, $subject): bool
    {
        return in_array($attribute, ['PAGE_ACCESS'])
            && $subject instanceof PageModel;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        $pageUrl = $subject->getPageUrl();

        if (!$user instanceof UserInterface) {
            return in_array($pageUrl, $this->rolesPages['Visitor']);
        }

        // Тут можна додати додаткову перевірку, наприклад, на основі ролі користувача в базі даних
        return in_array($pageUrl, $this->rolesPages['Regular User']);
    }
}
