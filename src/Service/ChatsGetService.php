<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Messages;
use App\Entity\Chat;
use App\Entity\User;
use App\Paginator\Paginator;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\SerializerInterface;

class ChatsGetService
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly SerializerInterface $serializer,
        private readonly Paginator $paginator,
    ){}

    public function getChats(int|string $page,User $user, string $path) :array
    {
        $perPage = 20;
        $offset = ($page - 1) * $perPage;

        $chats = $this->entityManager
            ->getRepository(Chat::class)
            ->createQueryBuilder('c')
            ->where('c.initiator = :userId OR c.companion = :userId')
            ->setParameter('userId', $user->getId())
            ->orderBy('c.updatedAt', 'DESC')
            ->setFirstResult($offset)
            ->setMaxResults($perPage)
            ->getQuery()
            ->getResult();

        $chats = $this->serializer->serialize($chats, 'json', ['groups' => 'chat:read']);
        $chats = json_decode($chats, true);
        
        $totalItems = $this->entityManager
                ->getRepository(Chat::class)
                ->createQueryBuilder('c')
                ->select('COUNT(c.id)')
                ->where('c.initiator = :userId OR c.companion = :userId')
                ->setParameter('userId', $user->getId())
                ->getQuery()
                ->getSingleScalarResult();


        if (!$chats) {
           return [];
        }
    

        foreach ($chats as $chat) {
            //Should be made via deserialization. Temporary solution.
            $chatForQuery =  $this->entityManager->getRepository(Chat::class)->findOneBy(['uuid' => $chat['uuid']]);
            //Should be made via deserialization. Temporary solution.

            $lastMessage = $this->entityManager->getRepository(Messages::class)->findOneBy([
                'chat' => $chatForQuery,
            ],
                ['createdAt' => 'DESC']
            );
            $lastMessageDate = $lastMessage?->getCreatedAt();
            $userNotMe = $chat['initiator']['uuid'] === $user->getUuid() ? $chat['companion'] : $chat['initiator'];
            $countUnread = $this->entityManager->getRepository(Messages::class)->count([
                'chat' => $chat,
                'status' => 'unread',
                'user' => $userNotMe,
            ]);
            $lastMessage = $this->serializer->serialize($lastMessage, 'json', ['groups' => 'messages:read']);

            $lastMessage = json_decode($lastMessage, true);

            $chat['lastMessage'] = $lastMessage;
            $chat['lastMessageDate'] = $lastMessageDate;
            $chat['countUnread'] = $countUnread;
            $newChats[] = $chat;
        }
        
        $answer = [];
        $answer['hydra:totalItems'] = $totalItems;
        $answer['hydra:member'] = $newChats;
        
        $url =  $path . '?page=';

        $view = $this->paginator->getHydraParody($url,$perPage,$totalItems,$page);
        $answer["hydra:view"] = $view;

        return $answer;
    }
}
