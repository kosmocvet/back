<?php

namespace App\Service;

use Psr\Log\LoggerInterface;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Twig\Environment;

/**
 * EmailSender service class.
 */
class EmailSender
{
    private ?Address $from = null;

    private Address $to;

    private string $subject = 'Welcome to Depo Magazines - Your Registration Details';

    private string $htmlTemplate;

    private array $context;
    /**
     * @var string|mixed
     */
    private string $defaultFrom;
    /**
     * @var string|mixed
     */
    private string $defaultFromName;

    private mixed $apiKey;

    public function __construct(
        private MailerInterface $mailer,
        private LoggerInterface $logger,
        private Environment $twig,
    ) {
        $this->defaultFrom = $_ENV['NO_REPLY_EMAIL'];
        $this->defaultFromName = $_ENV['NO_REPLY_NAME'];
        $this->apiKey = $_ENV['BREVO_API_KEY'];
    }

    /**
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function send(): void
    {
        if (str_contains($this->getTo()->getAddress(), '.xxx.')) {
            return;
        }

        $ch = curl_init();

        $data = [
            'sender' => [
                'name' => $this->defaultFromName,
                'email' => $this->defaultFrom,
            ],
            'to' => [
                [
                    'email' => $this->getTo()->getAddress(),
                    'name' => $this->getTo()->getName(),
                ],
            ],
            'subject' => $this->getSubject(),
            'htmlContent' => $this->twig->render($this->getHtmlTemplate(), $this->getContext()),
        ];

        curl_setopt($ch, CURLOPT_URL, 'https://api.brevo.com/v3/smtp/email');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));

        $headers = [];
        $headers[] = 'Accept: application/json';
        $headers[] = "Api-Key: {$this->apiKey}";
        $headers[] = 'Content-Type: application/json';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        try {
            curl_exec($ch);
            if (curl_errno($ch)) {
                echo 'Error:'.curl_error($ch);
            }
            curl_close($ch);
        } catch (TransportExceptionInterface $e) {
            $this->logger->error('Email error', ['error object' => $e]);
        }
    }

    /**
     * @return $this
     */
    public function context(array $context): static
    {
        $this->setContext($context);

        return $this;
    }

    /**
     * @return $this
     */
    public function htmlTemplate(string $htmlTemplate): static
    {
        $this->setHtmlTemplate($htmlTemplate);

        return $this;
    }

    /**
     * @return $this
     */
    public function subject(string $subject): static
    {
        $this->setSubject($subject);

        return $this;
    }

    /**
     * @return $this
     */
    public function to(Address $address): static
    {
        $this->setTo($address);

        return $this;
    }

    /**
     * @return $this
     */
    public function from(Address $address): static
    {
        $this->setFrom($address);

        return $this;
    }

    public function getFrom(): Address
    {
        return $this->from;
    }

    public function setFrom(Address $from): void
    {
        $this->from = $from;
    }

    public function getTo(): Address
    {
        return $this->to;
    }

    public function setTo(Address $to): void
    {
        $this->to = $to;
    }

    public function getSubject(): string
    {
        return $this->subject;
    }

    public function setSubject(string $subject): void
    {
        $this->subject = $subject;
    }

    public function getHtmlTemplate(): string
    {
        return $this->htmlTemplate;
    }

    public function setHtmlTemplate(string $htmlTemplate): void
    {
        $this->htmlTemplate = $htmlTemplate;
    }

    public function getContext(): array
    {
        return $this->context;
    }

    public function setContext(array $context): void
    {
        $this->context = $context;
    }
}
