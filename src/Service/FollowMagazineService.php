<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Followers;
use App\Entity\Magazine;
use App\Entity\User;
use App\Event\Followers\FollowersFollowEvent;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class FollowMagazineService
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly SerializerInterface $serializer,
        private readonly EventDispatcherInterface $eventDispatcher,
    ){}

    public function follow(string $magazineFromRequest, User $user) :JsonResponse
    {
        $magazineToFollow = $this->entityManager->getRepository(Magazine::class)->findOneBy([
            'uuid' => $magazineFromRequest,
        ]);

        $follower = $this->entityManager->getRepository(Followers::class)->findOneBy([
            'user' => $user,
            'followedMagazine' => $magazineToFollow,
        ]);

        
        if (null === $follower) {

            $followers = new Followers();
            $followers->setUser($user);
            $followers->setFollowedMagazine($magazineToFollow);
            $this->entityManager->persist($followers);
            $this->entityManager->flush();

            $event = new FollowersFollowEvent($user,$this->entityManager,$magazineToFollow);
            $this->eventDispatcher->dispatch($event, 'followers.follow');

            return new JsonResponse(['success' => true], 200);

        } else {
            return new JsonResponse(['message' => 'You are already following this magazine.'], 404);
        }

    }
}
