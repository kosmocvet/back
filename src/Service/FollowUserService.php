<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Followers;
use App\Entity\User;
use App\Event\Followers\FollowersFollowEvent;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class FollowUserService
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly SerializerInterface $serializer,
        private readonly EventDispatcherInterface $eventDispatcher,
    ){}

    public function follow(string $userFromRequest, User $user) :JsonResponse
    {
        $userToFollow = $this->entityManager->getRepository(User::class)->findOneBy([
            'uuid' => $userFromRequest,
        ]);

        $follower = $this->entityManager->getRepository(Followers::class)->findOneBy([
            'user' => $user,
            'followedUser' => $userToFollow,
        ]);

        
        if (null === $follower) {

            $followers = new Followers();
            $followers->setUser($user);
            $followers->setFollowedUser($userToFollow);
            $this->entityManager->persist($followers);
            $this->entityManager->flush();

            $event = new FollowersFollowEvent($user,$this->entityManager,$userToFollow);
            $this->eventDispatcher->dispatch($event, 'followers.follow');

            return new JsonResponse(['success' => true], 200);

        } else {
            return new JsonResponse(['message' => 'You are already following this user.'], 404);
        }

    }
}
