<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Chat;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\SerializerInterface;

class GetInterlocutorService
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly SerializerInterface $serializer,
    ){}

    public function getInterlocutor(string $userIdentifier, string $chatuuid) :array|JsonResponse
    {

        $user = $this->entityManager->getRepository(User::class)->findOneBy([
            'email' => $userIdentifier,
        ]);

        if (!$user) {
            return new JsonResponse('user is not found', 404);
        }
        
        $chat = $this->entityManager->getRepository(Chat::class)->findOneBy([
            'uuid' => $chatuuid,
        ]);

        if (!$chat) {
            return new JsonResponse('chat is not found', 404);
        }

       if ($chat->getInitiator() === $user) {
            $companion = $this->serializer->serialize($chat->getCompanion() , 'json', ['groups' => 'interlocutor:read']);
            $companion = json_decode($companion,true);
            return new JsonResponse($companion, 200);
       }
       elseif($chat->getCompanion() === $user)
       {
            $initiator = $this->serializer->serialize($chat->getInitiator() , 'json', ['groups' => 'interlocutor:read']);
            $initiator = json_decode($initiator,true);
            return new JsonResponse($initiator, 200);
       }
       else return new JsonResponse('unpredictable mistake', 404); 

    }
}
