<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Magazine;
use App\Entity\Media;
use App\Entity\Subscription;
use App\Entity\SubscriptionPlan;
use App\Trait\SubscribeTrait;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class MagazineUpdateService
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager
    ){}

    use SubscribeTrait;

    public function updateMagazine(array $data,Magazine $magazine) :JsonResponse
    {
        if (isset($data['cover'])) {
            if ($data['cover'] == null) {
                $magazine->setCover(null);
            }
            else
            {
                $media = $this->entityManager->getRepository(Media::class)->findOneBy(['uuid' => $data['cover']]);
                if (!$media) return new JsonResponse('Any media with that uuid',404); 
                $magazine->setCover($media);
            }
        }
        if (isset($data['about'])) {
            $magazine->setAbout($data['about']);
        }
        if (isset($data['subscription'])) { 
            $sub =  $this->entityManager->getRepository(Subscription::class)
            ->createQueryBuilder('s')
            ->join('s.magazine', 'u')
            ->where('u.uuid = :magazineId')
            ->andWhere('s.status = :active')
            ->setParameter('active', 'active')
            ->setParameter('magazineId', $magazine->getUuid())
            ->getQuery()
            ->getOneOrNullResult();

            if(!$sub) return new JsonResponse('Any active subscriptions for this magazine',404);
            if($data['subscription'] === 'CANCEL') $this->subRemove($sub,$this->entityManager);
            else{
                $plan = $this->entityManager->getRepository(SubscriptionPlan::class)->findOneBy(['uuid' => $data['subscription']]);
                if(!$plan) return new JsonResponse('Plan with that uuid is not exists',404);
                $this->newSub($sub,$this->entityManager,$magazine,$plan);
            }

        }
        if (isset($data['name'])) {
            $magazine->setName($data['name']);
        }
        if (isset($data['aboutPicture'])) {
            if ($data['aboutPicture'] == null) {
                $magazine->setAboutPicture(null);
            }
            else{
                $media = $this->entityManager->getRepository(Media::class)->findOneBy(['uuid' => $data['aboutPicture']]);
                if (!$media) return new JsonResponse('Any media with that uuid',404); 
                $magazine->setAboutPicture($media);
            }
        }
        if (isset($data['website'])) {
            $magazine->setWebsite($data['website']);
        }
        if (isset($data['instagram'])) {
            $magazine->setInstagram($data['instagram']);
        }
        if (isset($data['tiktok'])) {
            $magazine->setTiktok($data['tiktok']);
        }
        if (isset($data['pinterest'])) {
            $magazine->setPinterest($data['pinterest']);
        }
        if (isset($data['primary_contact'])) {
            $magazine->setPrimaryContact($data['primary_contact']);
        }
        if (isset($data['phone_number'])) {
            $magazine->setPhoneNumber($data['phone_number']);
        }
        if (isset($data['primaryContactEmail'])) {
            $magazine->setPrimaryContactEmail($data['primaryContactEmail']);
        }
        if (isset($data['notifyMessages'])) {
            $magazine->setNotifyMessages($data['notifyMessages']);
        }
        if (isset($data['notifySubscribers'])) {
            $magazine->setNotifySubscribers($data['notifySubscribers']);
        }
        if (isset($data['notifyComments'])) {
            $magazine->setNotifyComments($data['notifyComments']);
        }

        $magazine->setUpdatedAt(new DateTime());

        $this->entityManager->persist($magazine);
        $this->entityManager->flush();

        return new JsonResponse('success', 200);
    }
}
