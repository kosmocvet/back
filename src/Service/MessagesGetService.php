<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Messages;
use App\Entity\Chat;
use App\Entity\User;
use App\Paginator\Paginator;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\SerializerInterface;

class MessagesGetService
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly SerializerInterface $serializer,
        private readonly Paginator $paginator,
    ){}

    public function getMessages(int|string $page,Chat $chat,User $user,string $chatId , string $path) :array
    {

        $perPage = 50;

        $totalItems = $this->entityManager
                ->getRepository(Messages::class)
                ->createQueryBuilder('m')
                ->select('COUNT(m.id)')
                ->where('m.chat = :chat')
                ->setParameter('chat', $chat)
                ->getQuery()
                ->getSingleScalarResult();
                
        $page === 'noPageSpecified' ? $page = ceil($totalItems / $perPage) : $page;
            
        $offset = ($page - 1) * $perPage;


        $messages = $this->entityManager
        ->getRepository(Messages::class)
            ->createQueryBuilder('m')
            ->where('m.chat = :chat')
            ->setParameter('chat', $chat)
            ->orderBy('m.createdAt', 'ASC')
            ->setFirstResult($offset)
            ->setMaxResults($perPage)
            ->getQuery()
            ->getResult();

        if (!$messages) {
           return [];
        }
    
        $messages = $this->serializer->serialize($messages, 'json', ['groups' => 'messages:read']);
        $messages = json_decode($messages, true);
        foreach ($messages as $key => $message) {
            if ($message['user']['uuid'] === $user->getUuid()->toRfc4122()) {
                $message['isMyMessage'] = true;
            } else {
                $message['isMyMessage'] = false;
            }
            $messages[$key] = $message;
        }
        
        $answer = [];
        $answer['hydra:totalItems'] = $totalItems;
        $answer['hydra:member'] = $messages;
        
        $url =  $path . '?chatId=' . $chatId . '&page=';
        
        $view = $this->paginator->getHydraParody($url,$perPage,$totalItems,$page);

        $answer["hydra:view"] = $view;

        return $answer;
    }
}
