<?php

namespace App\Service;

class PasswordGeneratorService
{
    public function generatePassword(): string
    {
        $uppercase = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $lowercase = 'abcdefghijklmnopqrstuvwxyz';
        $numbers = '0123456789';
        $specialChars = '!@#$%^&*()_-+=<>?';

        $allChars = $uppercase.$lowercase.$numbers.$specialChars;

        // Генерируем по одному символу из каждой категории
        $password = $uppercase[rand(0, strlen($uppercase) - 1)].
            $lowercase[rand(0, strlen($lowercase) - 1)].
            $numbers[rand(0, strlen($numbers) - 1)].
            $specialChars[rand(0, strlen($specialChars) - 1)];

        // Дополняем оставшиеся символы до 10
        while (strlen($password) < 10) {
            $password .= $allChars[rand(0, strlen($allChars) - 1)];
        }

        // Перемешиваем символы, чтобы порядок не был предсказуемым
        $password = str_shuffle($password);

        return $password;
    }
}
