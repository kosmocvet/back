<?php

namespace App\Service;

use App\Entity\Magazine;
use App\Entity\Media;
use App\Entity\Speciality;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Mime\Address;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class RegistrationrService
{
    public function __construct(
        private readonly ValidatorInterface $validator,
        private readonly EntityManagerInterface $manager,
        private UserPasswordHasherInterface $hasher,
        private readonly PasswordGeneratorService $passwordGeneratorService,
        private readonly EmailSender $emailSender,
    ) {
    }

    public function registration(array $data): array|string|JsonResponse
    {
        $user = new User();
        $user->setName($data['name'] ?? null);
        $user->setEmail($data['email'] ?? null);
        $user->setAccountEmail($data['email'] ?? null);
        $password = $this->passwordGeneratorService->generatePassword();
        $user->setPassword($this->hasher->hashPassword($user, $password));
        $user->setAbout($password);
        $user->setLocation($data['location'] ?? null);
        if (isset($data['specialities'])) {
            if (is_array($data['specialities'])) {
                foreach ($data['specialities'] as $speciality) {
                    /** @var Speciality $speciality */
                    $speciality = $this->manager->getRepository(Speciality::class)->findOneBy(
                        ['uuid' => $speciality]
                    );
                    $user->addSpeciality($speciality ?? null);
                }
            } else {
                /** @var Speciality $speciality */
                $speciality = $this->manager->getRepository(Speciality::class)->findOneBy(
                    ['uuid' => $speciality]
                );
                $user->addSpeciality($speciality ?? null);
            }
        }
        $user->setWebsite($data['website'] ?? null);
        $user->setInstagram($data['instagram'] ?? null);
        $user->setTiktok($data['tiktok'] ?? null);
        $user->setPinterest($data['pinterest'] ?? null);
        $user->setStatus('offline');
        // if (isset($data['media'])) {
        //     $media = $this->manager->getRepository(Media::class)->findOneBy(
        //         ['uuid' => $data['media']]
        //     );
        // }
        // $user->setMedia($media ?? null);
        // if (isset($data['magazine'])) {
        //     /** @var Magazine $magazine */
        //     $magazine = $this->manager->getRepository(Magazine::class)->findOneBy(
        //         ['uuid' => $data['magazine']]
        //     );
        //     $user->setMagazine($magazine ?? null);
        // }

        $errors = $this->validator->validate($user);

        if (count($errors) > 0) {
            foreach ($errors as $violation) {
                // $violation->getPropertyPath() дает вам имя свойства
                // $violation->getMessage() дает вам текст ошибки для этого свойства
                $errorMessages[$violation->getPropertyPath()] = $violation->getMessage();
            }

            return $errorMessages;
        }

        $this->manager->persist($user);
        $this->manager->flush();

        $this->emailSender
            ->to(new Address($user->getEmail(), $user->getName()))
            ->subject('Confirm your registration on Depo')
            ->htmlTemplate('email/registration_confirm.html.twig')
            ->context([
                'title' => 'Confirm your registration on XaIndex Platform',
                'name' => $user->getName(),
                'accountEmail' => $user->getAccountEmail(),
                'password' => $password,
            ])
            ->send();

        return 'success';
    }
}
