<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Followers;
use App\Entity\Magazine;
use App\Entity\User;
use App\Event\Followers\FollowersFollowEvent;
use App\Event\Followers\FollowersUnFollowEvent;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class UnFollowMagazineService
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly SerializerInterface $serializer,
        private readonly EventDispatcherInterface $eventDispatcher,
    ){}

    public function unfollow(string $magazineFromRequest, User $user ) :JsonResponse
    {
        $magazineToUnFollow = $this->entityManager->getRepository(Magazine::class)->findOneBy([
            'uuid' => $magazineFromRequest,
        ]);

        $follower = $this->entityManager->getRepository(Followers::class)->findOneBy([
            'user' => $user,
            'followedMagazine' => $magazineToUnFollow,
        ]);

        
        if (null !== $follower) {

            $event = new FollowersUnFollowEvent($user,$this->entityManager,$magazineToUnFollow);
            $this->eventDispatcher->dispatch($event, 'followers.unfollow');

            $this->entityManager->remove($follower);
            $this->entityManager->flush();

            return new JsonResponse(['success' => true], 200);

        } else {
            return new JsonResponse(['message' => 'You are not following this magazine.'], 404);
        }

    }
}
