<?php

namespace App\Service;

use App\Entity\Magazine;
use App\Entity\Media;
use App\Entity\Speciality;
use App\Entity\Subscription;
use App\Entity\SubscriptionPlan;
use App\Entity\User;
use App\Trait\SubscribeTrait;
use DateTime;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class UpdateUserService
{
    public function __construct(
        private readonly ValidatorInterface $validator,
        private readonly EntityManagerInterface $manager,
        private UserPasswordHasherInterface $hasher,
        private readonly Security $security,
    ) {
    }

    use SubscribeTrait;

    public function update(array $data): array|string|JsonResponse
    {
        if (!$this->security->getUser()) {
            return new AccessDeniedHttpException('Access Denied');
        }
        $user = $this->manager->getRepository(User::class)->findOneBy([
            'email' => $this->security->getUser()->getUserIdentifier(),
        ]);

        if (isset($data['notifySubscribers'])) {
            $user->setNotifySubscribers($data['notifySubscribers']);
        }
        if (isset($data['notifyMessages'])) {
            $user->setNotifyMessages($data['notifyMessages']);
        }
        if (isset($data['name'])) {
            $user->setName($data['name']);
        }
        if (isset($data['email'])) {
            $user->setEmail($data['email']);
        }
        if (isset($data['plainPassword'])) {
            $user->setPassword($this->hasher->hashPassword($user, $data['plainPassword']));
        }
        if (isset($data['about'])) {
            $user->setAbout($data['about']);
        }
        if (isset($data['subscription'])) { 
            $sub =  $this->manager->getRepository(Subscription::class)
            ->createQueryBuilder('s')
            ->join('s.user', 'u')
            ->where('u.uuid = :userId')
            ->andWhere('s.status = :active')
            ->setParameter('active', 'active')
            ->setParameter('userId', $user->getUuid())
            ->getQuery()
            ->getOneOrNullResult();

            if(!$sub) return 'Any active subscriptions for this user';
            if($data['subscription'] === 'CANCEL') $this->subRemove($sub,$this->manager);
            else{
                $plan = $this->manager->getRepository(SubscriptionPlan::class)->findOneBy(['uuid' => $data['subscription']]);
                if(!$plan) return 'Plan with that uuid is not exists';
                $this->newSub($sub,$this->manager,$user,$plan);
            }

        }
        if (isset($data['location'])) {
            $user->setLocation($data['location']);
        }
        if (isset($data['specialities'])) {
            foreach ($user->getSpecialities() as $item) {
                $user->removeSpeciality($item);
            }
            if (is_array($data['specialities'])) {
                foreach ($data['specialities'] as $speciality) {
                    /** @var Speciality $speciality */
                    $speciality = $this->manager->getRepository(Speciality::class)->findOneBy(
                        ['uuid' => $speciality]
                    );
                    if (null !== $speciality) {
                        $user->addSpeciality($speciality);
                    }
                }
            } else {
                /** @var Speciality $speciality */
                $speciality = $this->manager->getRepository(Speciality::class)->findOneBy(
                    ['uuid' => $speciality]
                );
                if (null !== $speciality) {
                    $user->addSpeciality($speciality);
                }
            }
        }
        if (isset($data['website'])) {
            $user->setWebsite($data['website']);
        }
        if (isset($data['instagram'])) {
            $user->setInstagram($data['instagram']);
        }
        if (isset($data['tiktok'])) {
            $user->setTiktok($data['tiktok']);
        }
        if (isset($data['pinterest'])) {
            $user->setPinterest($data['pinterest']);
        }
        if (isset($data['avatar'])) {
            $avatar = $this->manager->getRepository(Media::class)->findOneBy(
              [
                'uuid' => $data['avatar']
              ]);
            if (null !== $avatar) {
                $user->setAvatar($avatar);
            }
        }
        if (isset($data['magazine'])) {
            /** @var Magazine $magazine */
            $magazine = $this->manager->getRepository(Magazine::class)->find($data['magazine']);
            if (null !== $magazine) {
                $user->setMagazine($magazine);
            }
        }

        if (isset($data['media'])) {
            $media = $this->manager->getRepository(Media::class)->find($data['media']);
            if (null !== $media) {
                $user->setMedia($media);
            }
        }

        $errors = $this->validator->validate($user);

        if (count($errors) > 0) {
            foreach ($errors as $violation) {
                // $violation->getPropertyPath() дает вам имя свойства
                // $violation->getMessage() дает вам текст ошибки для этого свойства
                $errorMessages[$violation->getPropertyPath()] = $violation->getMessage();
            }

            return $errorMessages;
        }

        $this->manager->persist($user);
        $this->manager->flush();

        return 'success';
    }
}
