<?php

namespace App\Service;

use App\Entity\ApiToken;
use App\Entity\User;
use App\Repository\ApiTokenRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Security;

class UserTokenService
{
    private ?ApiToken $activeToken;
    private $user;
    private $apiTokenLifetime;

    public function __construct(
        private ApiTokenRepository $apiTokenRepository,
        private EntityManagerInterface $em,
        private Security $security,
    ) {
        $this->user = $this->security->getUser();
        $this->apiTokenLifetime = $_ENV['API_TOKEN_LIFETIME'];
    }

    // TODO remove setting user into autowiring
    public function getActiveToken(?User $user = null, $flush = true): ApiToken
    {
        $this->user = $user ? $user : $this->security->getUser();
        if ($this->hasActiveTokenFromStorage()) {
            return $this->activeToken;
        }

        return $this->createActiveToken($flush);
    }

    public function hasActiveTokenFromStorage(): bool
    {
        if ($this->user->getId() === null) {
            return false;
        }

        return (bool)$this->activeToken = $this->apiTokenRepository->findActiveTokenByUser($this->user);
    }

    private function createActiveToken(bool $flush = true): ApiToken
    {
        $activeToken = new ApiToken();
        $activeToken->setUser($this->user);
        $activeToken->setToken($this->generateToken());
        $activeToken->setExpiredAt($this->getExpiredAt());
        $this->em->persist($activeToken);

        if ($flush) {
            $this->em->flush();
        }

        return $activeToken;
    }

    public function generateToken(): string
    {
        return bin2hex(random_bytes(60));
    }

    private function getExpiredAt(): DateTime
    {
        return (new DateTime())->add(new \DateInterval('PT' . $this->getTokenLifetime() . 'S'));
    }

    private function getTokenLifetime(): string
    {
        return $this->apiTokenLifetime ?: '604800';
    }
}