<?php

namespace App\State;

use ApiPlatform\Doctrine\Orm\State\CollectionProvider;
use ApiPlatform\Doctrine\Orm\State\ItemProvider;
use ApiPlatform\Metadata\CollectionOperationInterface;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\Entity\Photo;
use Symfony\Component\DependencyInjection\Attribute\Autowire;

class PhotoProvider implements ProviderInterface
{
    public function __construct(
        #[Autowire(service: ItemProvider::class)] private ProviderInterface $itemProvider,
        #[Autowire(service: CollectionProvider::class)] private ProviderInterface $collectionProvider,
    ) {
    }

    public function provide(Operation $operation, array $uriVariables = [], array $context = []): object|array|null
    {
        if ($operation instanceof CollectionOperationInterface) {
//            dd($this->collectionProvider->provide($operation, $uriVariables, $context));
            return $this->collectionProvider->provide($operation, $uriVariables, $context);
        }
        $treasure = $this->itemProvider->provide($operation, $uriVariables, $context);
        if (!$treasure instanceof Photo) {
            return $treasure;
        }

        return $treasure;
    }
}
