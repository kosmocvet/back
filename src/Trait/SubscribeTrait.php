<?php

namespace App\Trait;

use App\Entity\Magazine;
use App\Entity\Subscription;
use App\Entity\SubscriptionPlan;
use App\Entity\User;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;

trait SubscribeTrait {

    private function subRemove(Subscription $subscription,EntityManagerInterface $manager)
    {
        $subscription->setStatus('canceled');
        $subscription->setCanceledAt(new DateTimeImmutable());
        $manager->persist($subscription);
        $manager->flush();
    }

    private function newSub(Subscription $subscription,EntityManagerInterface $manager,User|Magazine $entity,SubscriptionPlan $plan)
    {
        $this->subRemove($subscription,$manager);
        
        $newSubscription = new Subscription();

        $newSubscription->setStatus('active');
        $newSubscription->setPlan($plan);
        $newSubscription->setStartedAt(new DateTimeImmutable());
        $newSubscription->setRenews(new DateTimeImmutable());

        if ($entity instanceof User) {
            $newSubscription->setUser($entity);
        }
        if ($entity instanceof Magazine) {
            $newSubscription->setMagazine($entity);
        }

        $manager->persist($newSubscription);
        $manager->flush();

    }

}